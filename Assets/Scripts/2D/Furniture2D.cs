﻿using UnityEngine;

public class Furniture2D : Furniture {
    public delegate void BlockScrollAction();
    public static event BlockScrollAction OnBlockScroll = delegate { };
    public delegate void UnBlockScrollAction();
    public static event UnBlockScrollAction OnUnBlockScroll = delegate { };

    private Vector3 mousePos;
    private bool isPlaced = true;

    #region свойства
    public override float Length {
        get { return length; }
        protected set {
            length = value;
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = new Vector3(value, oldScale.y, oldScale.z);

        }
    }

    public override float Width {
        get { return width; }
        protected set {
            width = value;
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = new Vector3(oldScale.x, oldScale.y, value);
        }
    }

    public override float Height {
        get { return height; }
        protected set {
            height = value;
            UpdateYPos();
        }
    }

    public override float DistFromFloor {
        get { return distFromFloor; }
        protected set {
            distFromFloor = value;
            UpdateYPos();
        }
    }

    public Texture2D TopImage {
        get {
            return resource != null ? resource.TopImage :
              ResHolder.Instance.NoTexture;
        }
    }
    #endregion

    #region методы
    public override void Init(Room room, Vector3 position, FurnitureRes resource) {
        base.Init(room, position, resource);
        gameObject.GetComponent<MeshRenderer>().material.mainTexture = TopImage;

        isPlaced = false;
        Color colorPicker = GetComponent<MeshRenderer>().material.color;
        colorPicker.a = 150 / 255f;
        GetComponent<MeshRenderer>().material.color = colorPicker;
    }

    public override bool Init(Room room, FurnitureData data) {
        if (base.Init(room, data)) {
            gameObject.GetComponent<MeshRenderer>().material.mainTexture = TopImage;
            return true;
        }
        else {
            Destroy();
            return false;
        }
    }

    public void Place() {
        Deselect();
        isPlaced = true;
        Color colorPicker = GetComponent<MeshRenderer>().material.color;
        colorPicker.a = 1;
        GetComponent<MeshRenderer>().material.color = colorPicker;

    }

    protected override void Select() {
        if (!isSelected) {
            base.Select();
            HilightSelected();
        }
    }

    protected override void Deselect() {
        if (isSelected) {
            base.Deselect();
            Dehilight();
            OnUnBlockScroll();
        }
    }

    protected override void Update() {
        base.Update();

        if (!isPlaced) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.y = (distFromFloor + height) * 0.01f;
            transform.position = pos;
        }
    }

    private void UpdateYPos() {
        float newYPos = (distFromFloor + height) * 0.01f;
        Vector3 oldPos = gameObject.transform.position;
        gameObject.transform.position = new Vector3(oldPos.x, newYPos, oldPos.z);
    }

    private void OnMouseDown() {
        mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
    }

    private void OnMouseDrag() {
        if (isSelected) {
            Vector3 mousePos2 = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
            Vector3 moveVect = mousePos2 - mousePos;
            gameObject.transform.position += moveVect;
            mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
        }
    }

    public override void OnMouseOver() {
        if (isPlaced)
            base.OnMouseOver();

        if (isSelected || !isPlaced) {
            OnBlockScroll();
            if (Input.GetAxis("Mouse ScrollWheel") != 0) {
                float speed = 10;

#if UNITY_EDITOR
                speed = 50;
#endif

                float angle = Input.GetAxis("Mouse ScrollWheel") * speed;
                transform.Rotate(Vector3.up, angle);
            }
        }
    }

    private void OnMouseExit() {
        if (isSelected)
            OnUnBlockScroll();
    }

    private void HilightPointed() {
        Color colorPicker = new Color(0.97f, 0.97f, 0.97f);
        gameObject.GetComponent<MeshRenderer>().material.color = colorPicker;
    }

    private void HilightSelected() {
        Color colorPicker = new Color(0.95f, 0.95f, 0.95f);
        gameObject.GetComponent<MeshRenderer>().material.color = colorPicker;
    }

    private void Dehilight() {
        Color colorPicker = new Color(1f, 1f, 1f);
        gameObject.GetComponent<MeshRenderer>().material.color = colorPicker;
    }
    #endregion
}
