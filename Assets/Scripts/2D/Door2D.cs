﻿using UnityEngine;

public class Door2D : WallOpening2D {
    private DoorLines lines;

    #region свойства
    public override float DefaultWidth {
        get { return Data2D.Instance.DefaultDoorWidth; }
    }

    public override float DefaultHeight {
        get { return Data2D.Instance.DefaultDoorHeight; }
    }

    public override Material Material {
        get { return ResHolder.Instance.DoorMaterial; }
    }

    public override Material MaterialSelected {
        get { return ResHolder.Instance.DoorSelectedMaterial; }
    }

    public override bool InsertOn {
        get { return Data2D.Instance.DoorInsertOn; }
        set { Data2D.Instance.DoorInsertOn = value; }
    }
    #endregion

    #region методы
    public override void Init(Wall2D wall, Vector3 pos, float height = 0f) {
        base.Init(wall, pos, height);
        gameObject.name = "door " + wall.Openings.Count;
    }

    public override void UpdateMesh() {
        base.UpdateMesh();
        lines.Redraw();
    }

    protected override bool SetWidth(float value) {
        if (value < 0.4f) {
            ModalPanel.Instance.Warning("Ширина двери не может быть меньше 40 см!");
            return false;
        }
        else {
            return base.SetWidth(value);
        }
    }

    protected override bool SetHeight(float value) {
        if (value < 1.5f) {
            ModalPanel.Instance.Warning("Высота двери не может быть меньше 150 см!");
            return false;
        }
        else if (value > Data2D.Instance.WallsHeight) {
            ModalPanel.Instance.Warning("Верхний край двери не может быть выше потолка!");
            return false;
        }
        else {
            height = value;
            return true;
        }
    }

    protected override void InitLines() {
        lines = new DoorLines(this);
    }

    protected override void ShowParamWindow() {
        RectTransform[] panels = new RectTransform[2];
        for (int i = 0; i < panels.Length; i++)
            panels[i] = Instantiate(ResHolder.Instance.ParamInputPanelPrefab).GetComponent<RectTransform>();

        panels[0].GetComponent<ParamInputPanel>().Init("Ширина ", Width, "см", 1, SetWidth);
        panels[1].GetComponent<ParamInputPanel>().Init("Высота", Height, "см", 1, SetHeight);

        ParamDialogWindow.Instance.Open("Параметры двери",
            ResHolder.Instance.DoorPictures[TextureID], panels, Destroy);
    }

    // линии контура двери
    private class DoorLines {
        private Door2D door2d;
        private LineRenderer sideLine1;
        private LineRenderer sideLine2;
        private LineRenderer doorLeafLine;
        private LineRenderer doorLeafArc;

        public DoorLines(Door2D door2d) {
            this.door2d = door2d;
            GameObject[] lineGOs = new GameObject[4];
            LineRenderer[] lines = new LineRenderer[4];
            for (int i = 0; i < lineGOs.Length; i++) {
                lineGOs[i] = new GameObject("DoorLine");
                lineGOs[i].transform.parent = door2d.gameObject.transform;
                lines[i] = lineGOs[i].AddComponent<LineRenderer>();
                lines[i].SetWidth(0.01f, 0.01f);
            }
            float thickness = Data2D.Instance.PlanLinesThickness;
            sideLine1 = lines[0];
            sideLine1.material = ResHolder.Instance.BlackLineMaterial;
            sideLine1.SetWidth(thickness, thickness);
            sideLine2 = lines[1];
            sideLine2.material = ResHolder.Instance.BlackLineMaterial;
            sideLine2.SetWidth(thickness, thickness);
            doorLeafLine = lines[2];
            doorLeafLine.material = ResHolder.Instance.GrayLineMaterial;
            doorLeafLine.SetWidth(thickness, thickness);
            doorLeafArc = lines[3];
            doorLeafArc.material = ResHolder.Instance.GrayLineMaterial;
            doorLeafArc.SetWidth(thickness, thickness);
        }

        public void Redraw() {
            Vector3 startPoint, endPoint, heightVect, normVect, normPerp;
            heightVect = new Vector3(0f, 0.004f, 0f);
            normVect = (door2d.EndPointPos - door2d.StartPointPos);
            normVect /= normVect.magnitude;
            normPerp = Vector3.Cross(-1 * RoomData.Instance.Normal, normVect);
            normPerp /= normPerp.magnitude;

            startPoint = door2d.StartPointPos + heightVect;
            endPoint = startPoint + normPerp * door2d.thickness;
            sideLine1.DrawLine(startPoint, endPoint);

            startPoint = door2d.EndPointPos + heightVect;
            endPoint = startPoint + normPerp * door2d.thickness;
            sideLine2.DrawLine(startPoint, endPoint);

            startPoint = door2d.StartPointPos + normPerp * door2d.thickness + heightVect;
            endPoint = door2d.StartPointPos + normPerp * (door2d.thickness + door2d.width) + heightVect;
            doorLeafLine.DrawLine(startPoint, endPoint);

            Vector3 center = door2d.StartPointPos + normPerp * door2d.thickness + heightVect;
            float radius = door2d.width;
            float startAngle = Math3DFunc.Angle360BetweenVectors(normPerp, Vector3.forward, Data2D.Instance.Normal);
            float arcAngle = 90f;
            doorLeafArc.DrawArc(center, radius, startAngle, arcAngle);
        }
    }
    #endregion
}
