﻿using UnityEngine;
using System.Collections.Generic;

public class Wall2D : MonoBehaviour {
    private List<WallOpening2D> openings;
    private LineRenderer[] lines;
    private LineDimension dimension; // размерная линия
    private MaterialRes materialRes;

    #region свойства
    public LineDimension Dimension {
        get { return dimension; }
    }

    // список проемов (дверей, окон) в стене
    public List<WallOpening2D> Openings {
        get { return openings; }
    }

    // номер стены
    public int Number {
        get { return Room.Walls.IndexOf(this); }
    }

    // контур помещения, в котором находится стена
    public Room2D Room { get; set; }

    // длина стены
    public float Length {
        get { return (EndPoint.Position - StartPoint.Position).magnitude; }
    }

    // начальная точка стены
    public Point2D StartPoint {
        get { return Room.Points[Number]; }
    }

    //конечная точка стены
    public Point2D EndPoint {
        get {
            int rightPointIdx = (Number + 1) % Room.Points.Count;
            return Room.Points[rightPointIdx];
        }
    }

    // нормализованный вектор направленный из нач. точки стены в конечную
    public Vector3 NormVect {
        get {
            Vector3 res = EndPoint.Position - StartPoint.Position;
            return res / res.magnitude;
        }
    }

    // нормализованный перпендикуляр к вектору стены
    public Vector3 NormPerp {
        get {
            Vector3 res = Vector3.Cross(-1 * RoomData.Instance.Normal, NormVect);
            return res / res.magnitude;
        }
    }

    // данные материала стены
    public MaterialRes MaterialRes {
        get { return materialRes; }
    }
    #endregion

    #region методы
    public void Init(Room2D room, MaterialRes materialRes) {
        Room = room;
        gameObject.name = "wall " + Room.Walls.Count;
        gameObject.transform.parent = room.gameObject.transform;
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshCollider>();
        gameObject.AddComponent<MeshRenderer>();
        gameObject.GetComponent<MeshRenderer>().material = ResHolder.Instance.WallMaterial;
        gameObject.layer = 10;
        Room.Walls.Add(this);
        StartPoint.Dimension.Redraw();
        EndPoint.Dimension.Redraw();

        openings = new List<WallOpening2D>();

        dimension = gameObject.AddComponent<LineDimension>();
        dimension.Init(1, true);
        if (Data2D.Instance.WallDimShowOn)
            dimension.Visible = true;
        else
            dimension.Visible = false;

        GameObject[] lineGOs = new GameObject[2];
        lineGOs[0] = new GameObject("OuterLine");
        lineGOs[1] = new GameObject("InnerLine");
        lines = new LineRenderer[2];
        for (int i = 0; i < 2; i++) {
            lineGOs[i].transform.parent = gameObject.transform;
            lines[i] = lineGOs[i].AddComponent<LineRenderer>();
            lines[i].material = ResHolder.Instance.BlackLineMaterial;
            float thickness = Data2D.Instance.PlanLinesThickness;
            lines[i].SetWidth(thickness, thickness);
        }

        this.materialRes = materialRes;
    }

    // обновить меш стены
    public void UpdateMesh() {
        Vector3 heightVect = new Vector3(0f, 0.002f, 0f);
        List<Vector3> meshPoints = new List<Vector3>(); // точки меша стены
        meshPoints.Add(StartPoint.Position + heightVect);
        meshPoints.Add(StartPoint.OuterPosition + heightVect);
        meshPoints.Add(EndPoint.OuterPosition + heightVect);
        meshPoints.Add(EndPoint.Position + heightVect);

        Mesh newMesh = MeshGenerator.GenerateMesh(meshPoints, true, RoomData.Instance.Normal, MeshTexMode.NoTexture);
        GetComponent<MeshFilter>().mesh = newMesh;
        gameObject.GetComponent<MeshCollider>().sharedMesh = newMesh;

        foreach (WallOpening2D opn in openings) {
            opn.UpdateMesh();
        }

        dimension.Redraw(StartPoint.Position, EndPoint.Position); // отрисовка размерной линии

        heightVect.y += 0.001f;
        // отрисовка внешней и внуртренней линии 
        lines[0].DrawLine(StartPoint.Position + heightVect, EndPoint.Position + heightVect);
        lines[1].DrawLine(StartPoint.OuterPosition + heightVect, EndPoint.OuterPosition + heightVect);
    }


    public bool DetectCollisions(Point2D point, Vector3 newPos) {
        Point2D otherPoint;
        if (StartPoint.Equals(point))
            otherPoint = EndPoint;
        else if (EndPoint.Equals(point))
            otherPoint = StartPoint;
        else
            return false;

        float newLength = (newPos - otherPoint.Position).magnitude;
        if (newLength < 0.2f)
            return true;
        if (openings.Count != 0) {
            WallOpening2D lastOpn = openings[openings.Count - 1];
            if (lastOpn.DistFromWallStartPoint + lastOpn.Width > newLength) {
                return true;
            }
        }
        return false;
    }

    public bool PreventCollisions(WallOpening2D newOpn) {
        if (newOpn.DefaultWidth >= Length)
            return true;

        if (newOpn.DistFromWallStartPoint < 0)
            newOpn.DistFromWallStartPoint = 0;
        if (newOpn.DistFromWallStartPoint > Length - newOpn.Width)
            newOpn.DistFromWallStartPoint = Length - newOpn.Width;

        foreach (WallOpening2D opn in openings) {
            if (!opn.Equals(newOpn)) {
                float dist = opn.DistFromWallStartPoint;
                float newOpnDist = newOpn.DistFromWallStartPoint;
                if (newOpnDist > dist && newOpnDist < dist + opn.Width) {
                    newOpn.DistFromWallStartPoint = dist + opn.Width + 0.01f;
                    return true;
                }

                if (newOpnDist + newOpn.Width > dist && newOpnDist + newOpn.Width < dist + opn.Width) {
                    newOpn.DistFromWallStartPoint = dist - newOpn.Width - 0.01f;
                    return true;
                }
            }
        }
        return false;
    }

    public void Destroy() {
        for (int i = 0; i < openings.Count; i++)
            openings[i].Destroy();

        for (int i = 0; i < lines.Length; i++)
            Destroy(lines[i].gameObject);

        dimension.Destroy();

        Room.Walls.Remove(this);
        Destroy(gameObject);
    }

    private void InsertOpening<T>() where T : WallOpening2D {
        int layerMask = 1 << 10;
        Vector3 pos;
        if (MouseUtils.GetMouseWorldPosFromRaycast(layerMask, out pos)) {
            GameObject opnGO = new GameObject();
            T opn2d = opnGO.AddComponent<T>();
            opn2d.Init(this, pos);
            opn2d.InsertOn = false;

            if (PreventCollisions(opn2d)) {
                opn2d.InsertOn = true;
                opn2d.Destroy();
                ModalPanel.Instance.Warning("Недостаточно места для установки окна/двери!");
            }
        }
    }

    private void OnMouseDown() {
        if (Data2D.Instance.DoorInsertOn) {
            InsertOpening<Door2D>();
        }
        else if (Data2D.Instance.WindowInsertOn) {
            InsertOpening<Window2D>();
        }
    }
    private void OnMouseEnter() {
        if (Data2D.Instance.DoorInsertOn) {
            CursorImage.Instance.SetImage(23, 48, ResHolder.Instance.DoorIconColor, 10, 0);
        }
        else if (Data2D.Instance.WindowInsertOn) {
            CursorImage.Instance.SetImage(48, 48, ResHolder.Instance.WindowIconColor, 10, 0);
        }
    }

    private void OnMouseExit() {
        if (Data2D.Instance.DoorInsertOn) {
            CursorImage.Instance.SetImage(23, 48, ResHolder.Instance.DoorIconGrey, 10, 0);
        }
        else if (Data2D.Instance.WindowInsertOn) {
            CursorImage.Instance.SetImage(48, 48, ResHolder.Instance.WindowIconGrey, 10, 0);
        }
    }
    #endregion
}