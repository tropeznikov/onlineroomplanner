﻿using UnityEngine;

public class AngleDimension : MonoBehaviour {
    private Point2D point2d;
    private LineRenderer line;
    private DimensionText text;
    private float arcRadius = 0.28f;
    private float textDist = 0.16f;
    private bool visible;

    public bool Visible {
        get { return visible; }
        set {
            visible = value;
            line.gameObject.SetActive(value);
            text.Visible = value;
        }
    }

    public void Redraw() {
        float arcAngle;
        if (point2d.GetAngleBetweenNeighborWalls(out arcAngle)) {
            if (!Visible && Data2D.Instance.AngleDimShowOn)
                Visible = true;

            Vector3 heightVect = new Vector3(0f, 0.002f, 0f);
            Vector3 secondWallVect = point2d.GetNeighborWalls()[1].NormVect;
            float startAngle = Math3DFunc.Angle360BetweenVectors(secondWallVect, Vector3.forward, Data2D.Instance.Normal);
            line.DrawArc(point2d.transform.position + heightVect, arcRadius, startAngle, arcAngle);

            text.Position = Quaternion.Euler(0f, arcAngle / 2, 0f) * secondWallVect * textDist + point2d.Position;
            text.text = arcAngle.ToString("f0") + "°";
        }
    }

    public void Destroy() {
        Destroy(line.gameObject);
        text.Destroy();
    }

    void Awake() {
        point2d = gameObject.GetComponent<Point2D>();

        GameObject lineGO = new GameObject("AngleDimLine");
        lineGO.transform.parent = gameObject.transform;
        line = lineGO.AddComponent<LineRenderer>();
        line.material = ResHolder.Instance.BlackLineMaterial;
        float thickness = Data2D.Instance.DimLinesThickness;
        line.SetWidth(thickness, thickness);

        GameObject textGO = new GameObject("AngleDimText");
        text = textGO.AddComponent<DimensionText>();
        text.Init();

        // изначально угловой размер не показывается,
        // т.к. у только что созданной точки нет двух смежных стен
        Visible = false;
    }
}
