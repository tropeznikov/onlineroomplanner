﻿using UnityEngine;

public class Window2D : WallOpening2D {

    private float distOverFloor;
    private WindowLines lines;

    #region свойства
    public float DistOverFloor {
        get { return distOverFloor; }
    }

    public override float DefaultWidth {
        get { return Data2D.Instance.DefaultWindowWidth; }
    }

    public override float DefaultHeight {
        get { return Data2D.Instance.DefaultWindowHeight; }
    }

    public float DefaultDistOverFloor {
        get { return Data2D.Instance.DefaultWindowDistOverFloor; }
    }

    public override Material Material {
        get { return ResHolder.Instance.WindowMaterial; }
    }

    public override Material MaterialSelected {
        get { return ResHolder.Instance.WindowSelectedMaterial; }
    }

    public override bool InsertOn {
        get { return Data2D.Instance.WindowInsertOn; }
        set { Data2D.Instance.WindowInsertOn = value; }
    }
    #endregion

    #region методы
    public override void Init(Wall2D wall, Vector3 pos, float height = 0f) {
        base.Init(wall, pos, height);
        distOverFloor = DefaultDistOverFloor;
        gameObject.name = "window " + wall.Openings.Count;
    }

    public void Init(Wall2D wall, Vector3 pos, float height = 0f, float overFloor = 0f) {
        base.Init(wall, pos, height);
        distOverFloor = overFloor != 0 ? overFloor : DefaultDistOverFloor;
        gameObject.name = "window " + wall.Openings.Count;
    }

    public override void UpdateMesh() {
        base.UpdateMesh();
        lines.Redraw();
    }

    protected override bool SetWidth(float value) {
        if (value < 0.2f) {
            ModalPanel.Instance.Warning("Ширина окна не может быть меньше 20 см!");
            return false;
        }
        else {
            return base.SetWidth(value);
        }
    }

    protected override bool SetHeight(float value) {
        if (value < 0.2f) {
            ModalPanel.Instance.Warning("Высота окна не может быть меньше 20 см!");
            return false;
        }
        else if (value + distOverFloor > Data2D.Instance.WallsHeight) {
            ModalPanel.Instance.Warning("Верхний край окна не может быть выше потолка!");
            return false;
        }
        else {
            height = value;
            return true;
        }
    }

    protected override void InitLines() {
        lines = new WindowLines(this);
    }

    protected override void ShowParamWindow() {
        RectTransform[] panels = new RectTransform[3];
        for (int i = 0; i < panels.Length; i++)
            panels[i] = Instantiate(ResHolder.Instance.ParamInputPanelPrefab).GetComponent<RectTransform>();

        panels[0].GetComponent<ParamInputPanel>().Init("Ширина ", Width, "см", 1, SetWidth);
        panels[1].GetComponent<ParamInputPanel>().Init("Высота", Height, "см", 1, SetHeight);
        panels[2].GetComponent<ParamInputPanel>().Init("От пола", DistOverFloor, "см", 1, SetDistOverFloor);

        ParamDialogWindow.Instance.Open("Параметры окна", ResHolder.Instance.WindowPictures[TextureID], panels, Destroy);
    }

    private bool SetDistOverFloor(float value) {
        if (value + height > Data2D.Instance.WallsHeight) {
            ModalPanel.Instance.Warning("Верхний край окна не может быть выше потолка!");
            return false;
        }
        else {
            distOverFloor = value;
            return true;
        }
    }

    private class WindowLines {
        private Window2D window2d;
        private LineRenderer sideLine1;
        private LineRenderer sideLine2;
        private LineRenderer centerLine;

        public WindowLines(Window2D window2d) {
            this.window2d = window2d;
            GameObject[] lineGOs = new GameObject[3];
            LineRenderer[] lines = new LineRenderer[3];
            for (int i = 0; i < lineGOs.Length; i++) {
                lineGOs[i] = new GameObject("WindowLine");
                lineGOs[i].transform.parent = window2d.gameObject.transform;
                lines[i] = lineGOs[i].AddComponent<LineRenderer>();
                lines[i].material = ResHolder.Instance.BlackLineMaterial;
                float thickness = Data2D.Instance.PlanLinesThickness;
                lines[i].SetWidth(thickness, thickness);
            }
            sideLine1 = lines[0];
            sideLine2 = lines[1];
            centerLine = lines[2];
        }

        public void Redraw() {
            Vector3 startPoint, endPoint, heightVect, normVect, normPerp;
            heightVect = new Vector3(0f, 0.004f, 0f);
            normVect = (window2d.EndPointPos - window2d.StartPointPos);
            normVect /= normVect.magnitude;
            normPerp = Vector3.Cross(-1 * RoomData.Instance.Normal, normVect);
            normPerp /= normPerp.magnitude;

            startPoint = window2d.StartPointPos + heightVect;
            endPoint = startPoint + normPerp * window2d.thickness;
            sideLine1.DrawLine(startPoint, endPoint);

            startPoint = window2d.EndPointPos + heightVect;
            endPoint = startPoint + normPerp * window2d.thickness;
            sideLine2.DrawLine(startPoint, endPoint);

            startPoint = window2d.StartPointPos + normPerp * window2d.thickness / 2 + heightVect;
            endPoint = window2d.EndPointPos + normPerp * window2d.thickness / 2 + heightVect;
            centerLine.DrawLine(startPoint, endPoint);
        }
    }
    #endregion
}