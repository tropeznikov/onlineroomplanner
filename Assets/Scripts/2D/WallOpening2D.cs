﻿using UnityEngine;
using System.Collections.Generic;

public abstract class WallOpening2D : MonoBehaviour {
    protected float width;
    protected float height;
    protected float thickness;
    protected int textureID;
    private float distFromWallStartPoint;
    private LineDimension dimension;
    private bool isDragged = false;
    private Vector3 mousePos;

    #region свойства 
    public float DistFromWallStartPoint {
        get { return distFromWallStartPoint; }
        set { distFromWallStartPoint = value; }
    }

    public int Number {
        get { return Wall.Openings.IndexOf(this); }
    }

    public float Width {
        get { return width; }
    }

    public float Height {
        get { return height; }
    }

    public int TextureID {
        get { return textureID; }
    }

    public Wall2D Wall { get; private set; }

    // коордтнаты начальной точки
    public Vector3 StartPointPos {
        get { return Wall.StartPoint.Position + Wall.NormVect * distFromWallStartPoint; }
    }

    // координаты конечной точки
    public Vector3 EndPointPos {
        get { return Wall.StartPoint.Position + Wall.NormVect * (distFromWallStartPoint + width); }
    }

    public Vector3 StartPointNeighborPos {
        get {
            int idx = Number - 1;
            if (idx < 0)
                return Wall.StartPoint.Position;
            else
                return Wall.Openings[idx].EndPointPos;
        }
    }

    public Vector3 EndPointNeighborPos {
        get {
            int idx = Number + 1;
            if (idx >= Wall.Openings.Count)
                return Wall.EndPoint.Position;
            else
                return Wall.Openings[idx].StartPointPos;
        }
    }

    public float DefaultThickness {
        get { return Data2D.Instance.WallThickness; }
    }

    #region абстрактные свойства
    public abstract float DefaultWidth {
        get;
    }

    public abstract float DefaultHeight {
        get;
    }

    public abstract Material Material {
        get;
    }

    public abstract Material MaterialSelected {
        get;
    }

    public abstract bool InsertOn {
        get;
        set;
    }
    #endregion
    #endregion

    #region методы
    // инициализация
    public virtual void Init(Wall2D wall, Vector3 pos, float height = 0f) {
        Wall = wall;
        gameObject.transform.parent = Wall.gameObject.transform;
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshCollider>();
        gameObject.AddComponent<MeshRenderer>();
        gameObject.GetComponent<MeshRenderer>().material = Material;
        gameObject.layer = 10;

        width = DefaultWidth;
        this.height = height != 0 ? height : DefaultHeight;
        thickness = DefaultThickness;

        InitLines();

        SetPosition(pos);
        Wall.Openings.Add(this);
        Wall.Openings.Sort((n1, n2) => n1.DistFromWallStartPoint.CompareTo(n2.DistFromWallStartPoint));

        dimension = gameObject.AddComponent<LineDimension>();
        dimension.Init(3, false);
        dimension.Visible = false;

        textureID = 0;
    }

    public virtual void UpdateMesh() {
        Vector3 heightVect = new Vector3(0f, 0.003f, 0f);
        List<Vector3> meshPoints = new List<Vector3>();
        Vector3 perp = Wall.NormPerp * thickness;
        meshPoints.Add(StartPointPos + heightVect);
        meshPoints.Add(StartPointPos + perp + heightVect);
        meshPoints.Add(EndPointPos + perp + heightVect);
        meshPoints.Add(EndPointPos + heightVect);

        Mesh newMesh = MeshGenerator.GenerateMesh(meshPoints, true, RoomData.Instance.Normal, MeshTexMode.NoTexture);
        GetComponent<MeshFilter>().mesh = newMesh;
        GetComponent<MeshCollider>().sharedMesh = newMesh;
    }

    public void Destroy() {
        dimension.Destroy();
        Wall.Openings.Remove(this);
        Destroy(gameObject);
    }

    protected virtual bool SetWidth(float value) {
        bool valueIsCorrect = true;
        float newDist = distFromWallStartPoint + (width - value) / 2;

        // если есть соседний проем слева
        if (Number != 0) {
            WallOpening2D leftNeighbor = Wall.Openings[Number - 1];
            if (newDist < leftNeighbor.DistFromWallStartPoint + leftNeighbor.Width)
                valueIsCorrect = false;
        }
        else if (newDist < 0) {
            valueIsCorrect = false;
        }

        // если есть соседний проем справа
        if (valueIsCorrect && Number != Wall.Openings.Count - 1) {
            WallOpening2D rightNeighbor = Wall.Openings[Number + 1];
            if (newDist + value > rightNeighbor.DistFromWallStartPoint)
                valueIsCorrect = false;
        }
        else if (newDist + value > Wall.Length) {
            valueIsCorrect = false;
        }

        if (valueIsCorrect) {
            distFromWallStartPoint = newDist;
            width = value;
            UpdateMesh();
            return true;
        }
        else {
            ModalPanel.Instance.Warning("Ширина слишком большая!\nОкно/дверь не может " +
                "выходить за края стен\nили пересекать другие окна/двери");
            return false;
        }
    }

    protected abstract bool SetHeight(float value);

    protected abstract void InitLines();

    protected abstract void ShowParamWindow();

    // задать положение и перерисовать
    private void SetPosition(Vector3 point) {
        Vector3 pos = Math3DFunc.ProjectPointOnLineSegment(Wall.StartPoint.Position, Wall.EndPoint.Position, point);
        distFromWallStartPoint = (pos - Wall.StartPoint.Position).magnitude - width / 2;

        Wall.PreventCollisions(this);
        UpdateMesh();
    }

    private void ShowDimension() {
        Wall.Dimension.Visible = false;
        dimension.Visible = true;
        dimension.Redraw(StartPointNeighborPos, StartPointPos, EndPointPos, EndPointNeighborPos);
    }

    private void HideDimension() {
        dimension.Visible = false;
        if (Data2D.Instance.WallDimShowOn)
            Wall.Dimension.Visible = true;
    }

    private void OnMouseDown() {
        isDragged = true;
        mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
    }

    private void OnMouseDrag() {
        Vector3 mousePos2 = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
        Vector3 moveVect = mousePos2 - mousePos;
        SetPosition(StartPointPos + Wall.NormVect * width / 2 + moveVect);
        ShowDimension();
        mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
    }

    private void OnMouseUp() {
        if (isDragged) {
            isDragged = false;
            HideDimension();
        }
    }

    private void OnMouseEnter() {
        gameObject.GetComponent<MeshRenderer>().material = MaterialSelected;
    }

    private void OnMouseExit() {
        gameObject.GetComponent<MeshRenderer>().material = Material;
        if (!isDragged)
            HideDimension();
    }

    private void OnMouseOver() {
        ShowDimension();

        if (Input.GetMouseButtonDown(1)) {
            ShowParamWindow();
        }

        if (Input.GetKeyUp(KeyCode.Delete)) {
            HideDimension();
            Destroy();
        }
    }
    #endregion
}
