﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

// контур помещения
public class Room2D : Room {
    private List<Point2D> points; // точки контура помещения
    private List<Wall2D> walls; // стены помещения
    private bool shapeClosed = false; // замкнут ли контур
    private Vector3 normal; // нормаль к контуру помещения
    private DimensionText floorAreaText; // текстовое поле с площадью пола
    private bool catalogItemTaken = false;
    private FurnitureRes curFurnitRes;
    private Furniture2D curFurnit2D;
    private MaterialRes floorMaterialRes;

    #region свойства
    public List<Point2D> Points {
        get { return points; }
    }

    public List<Wall2D> Walls {
        get { return walls; }
    }

    public bool ShapeClosed {
        get { return shapeClosed; }
    }

    public MaterialRes FloorMaterialRes {
        get { return floorMaterialRes; }
    }
    #endregion

    #region методы
    // перерисовка контура пола и всех стен помещения
    public void RedrawShape() {
        foreach (Point2D point in points) {
            point.UpdateOuterPosition();
        }
        foreach (Wall2D wall in walls) {
            wall.UpdateMesh();
        }

        RedrawFloor();
    }

    // перерисовка контура пола и стен смежных с точками, которые являются соседями clickedPt
    // в качестве параметра clickedPt метод принимает точку, положение которой изменилось 
    public void RedrawShape(Point2D clickedPt) {
        List<Point2D> points = clickedPt.GetNeighborPoints();

        if (points.Count > 0) {
            clickedPt.UpdateOuterPosition();
            foreach (Point2D point in points) {
                point.UpdateOuterPosition();
                List<Wall2D> walls = point.GetNeighborWalls();
                foreach (Wall2D wall in walls) {
                    wall.UpdateMesh();
                }
            }
        }

        RedrawFloor();
    }

    private void RedrawFloor() {
        // если контур замкнут - актуализируем меш пола
        if (shapeClosed) {
            List<Vector3> floorPoints = new List<Vector3>();
            float height = 0.001f;
            foreach (Point2D point in points) {
                Vector3 pos = point.Position;
                pos.y = height;
                floorPoints.Add(pos);
            }

            Mesh mesh = MeshGenerator.GenerateMesh(floorPoints, true, normal, MeshTexMode.NoTexture);
            GetComponent<MeshFilter>().mesh = mesh;

            floorAreaText.Visible = true;
            floorAreaText.Position = mesh.bounds.center;
            //TODO: вычислять центр многоугольника (пола), и помещать туда текст с площадью пола
            floorAreaText.text = Math3DFunc.MeshArea(mesh).ToString("f1") + " м2";
        }
    }

    // "разворот" контура помещения в обратную сторону
    // применяется, если изначально контур был задан против часовой стрелки
    public void ReverseShape() {
        points.Reverse();
        walls.Reverse();
        foreach (Point2D point in points) {
            point.gameObject.name = "point" + point.Number;
            point.Dimension.Redraw();
        }

        foreach (Wall2D wall in walls)
            wall.gameObject.name = "wall" + wall.Number;
        RedrawShape();
    }

    // очистка контура помещения
    public void ClearShape() {
        int count;
        count = walls.Count;
        for (int i = 0; i < count; i++)
            walls[0].Destroy();

        count = points.Count;
        for (int i = 0; i < count; i++)
            points[0].Destroy();

        count = furniture.Count;
        for (int i = 0; i < count; i++)
            furniture[0].Destroy();

        shapeClosed = false;
        GetComponent<MeshFilter>().mesh.Clear();

        floorAreaText.Visible = false;
    }

    public void HideWallDimensions() {
        foreach (Wall2D wall in walls)
            wall.Dimension.Visible = false;
    }

    public void ShowWallDimensions() {
        foreach (Wall2D wall in walls)
            wall.Dimension.Visible = true;
    }

    public void HideAngleDimensions() {
        foreach (Point2D point in points)
            point.Dimension.Visible = false;
    }

    public void ShowAngleDimensions() {
        foreach (Point2D point in points)
            point.Dimension.Visible = true;
    }

    // добавление точки к контуру помещения
    private Point2D AddPoint(Vector3 pos) {
        GameObject newPointGO = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Point2D point2d = newPointGO.AddComponent<Point2D>();
        point2d.Init(this, pos);
        return point2d;
    }

    // добавление стены к контуру помещения
    private Wall2D AddWall(MaterialRes materialRes = null) {
        GameObject wallGO = new GameObject();
        Wall2D wall2d = wallGO.AddComponent<Wall2D>();
        wall2d.Init(this, materialRes);
        return wall2d;
    }

    // находим сумму углов между векторами стен
    // если сумма положительна, то точки контура заданы по часовой стрелке
    // если сумма отрицательна, то против часовой стрелки
    private float GetSumOfAngles() {
        float sum = 0;
        if (shapeClosed) {
            for (int i = 0; i < points.Count; i++) {
                Vector3 p0 = points[(points.Count + i - 1) % points.Count].Position;
                Vector3 p1 = points[i].Position;
                Vector3 p2 = points[(i + 1) % points.Count].Position;
                Vector3 v1 = p0 - p1;
                Vector3 v2 = p2 - p1;
                float angle = Math3DFunc.AngleBetweenVectors(v1, v2, normal);
                sum += angle;
            }
        }
        return sum;
    }

    private void LoadRoomFromData() {
        RoomData roomData = RoomData.Instance;
        if (!roomData.IsEmpty) {
            for (int i = 0; i < roomData.Walls.Length; i++) {
                WallData wallData = roomData.Walls[i];
                if (i == 0)
                    AddPoint(wallData.StartPointPos);
                if (i != roomData.Walls.Length - 1)
                    AddPoint(wallData.EndPointPos);
                else
                    shapeClosed = true;

                Wall2D wall2d = AddWall(wallData.MaterialRes);
                foreach (DoorData doorData in wallData.Doors) {
                    GameObject doorGO = new GameObject();
                    Door2D door2d = doorGO.AddComponent<Door2D>();
                    Vector3 pos = doorData.StartPointPos + (doorData.EndPointPos - doorData.StartPointPos) / 2;
                    door2d.Init(wall2d, pos, doorData.Height);
                }
                foreach (WindowData windowData in wallData.Windows) {
                    GameObject windowGO = new GameObject();
                    Window2D window2d = windowGO.AddComponent<Window2D>();
                    Vector3 pos = windowData.StartPointPos + (windowData.EndPointPos - windowData.StartPointPos) / 2;
                    window2d.Init(wall2d, pos, windowData.Height, windowData.DistOverFloor);
                }
            }
            RedrawShape();
            floorMaterialRes = roomData.FloorMaterialRes;

            foreach (FurnitureData furnitData in roomData.Furniture) {
                GameObject furnitGO = Instantiate(ResHolder.Instance.Model2D);
                Furniture2D furnit2d = furnitGO.AddComponent<Furniture2D>();
                furnit2d.Init(this, furnitData);
            }
        }
    }

    private void OnCatalogItemTaken(CatalogItem item) {
        catalogItemTaken = true;
        curFurnitRes = item as FurnitureRes;
    }

    private void DropCatalogItem() {
        catalogItemTaken = false;
        curFurnitRes = null;
        curFurnit2D = null;
    }

    private void Start() {
        normal = Data2D.Instance.Normal;
        points = new List<Point2D>();
        walls = new List<Wall2D>();
        furniture = new List<Furniture>();
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        GetComponent<MeshRenderer>().material = ResHolder.Instance.FloorMaterial;

        GameObject textGO = new GameObject("FloorAreaText");
        floorAreaText = textGO.AddComponent<DimensionText>();
        floorAreaText.Init();

        LoadRoomFromData();
    }

    private void Update() {
        // если кликнули ЛКМ, но не по UI
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject(-1)) {
            int planeLayerMask = 1 << 8; // слой для plane
            int pointsLayerMask = 1 << 9; // слой для точек контура
            int wallsLayerMask = 1 << 10; // слой для стен
            int furnitureLayerMask = 1 << 13; // слой для стен
            Vector3 pos;

            if (!catalogItemTaken && !shapeClosed) {
                // если кликнули на существующую точку
                if (MouseUtils.GetMouseWorldPosFromRaycast(pointsLayerMask, out pos)) {
                    // если кликнули на первую точку, то замыкаем контур
                    if (points.Count > 2 && points[0].GetComponent<Collider>().bounds.Contains(pos)) {
                        shapeClosed = true;
                        AddWall();
                        // если контур задан против часовой стрелки, то разворачиваем его в обратную сторону
                        if (GetSumOfAngles() < 0)
                            ReverseShape();
                        else
                            RedrawShape(points[0]);
                    }
                }
                // если кликнули по стене
                else if (MouseUtils.GetMouseWorldPosFromRaycast(wallsLayerMask, out pos)) {
                    return;
                }
                // если кликнули по мебели
                else if (MouseUtils.GetMouseWorldPosFromRaycast(furnitureLayerMask, out pos)) {
                    return;
                }
                //если кликнули по plane                    
                else if (MouseUtils.GetMouseWorldPosFromRaycast(planeLayerMask, out pos)) {
                    // округляем координаты точки до 0,1 м
                    pos.x = (float)Math.Round(pos.x, 1, MidpointRounding.AwayFromZero);
                    pos.z = (float)Math.Round(pos.z, 1, MidpointRounding.AwayFromZero);
                    // задаем высоту установки точек
                    pos.y = Data2D.Instance.RoomPointsPosY;

                    AddPoint(pos);
                    if (points.Count > 1) {
                        AddWall();
                        RedrawShape(points[points.Count - 1]);
                    }
                }
            }
        }

        if (catalogItemTaken) {
            if (curFurnit2D == null) {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pos.y = 0f;
                GameObject myObject = Instantiate(ResHolder.Instance.Model2D);
                curFurnit2D = myObject.AddComponent<Furniture2D>();
                curFurnit2D.Init(this, pos, curFurnitRes);
            }

            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject(-1)) {
                curFurnit2D.Place();
                DropCatalogItem();
            }
            else if (Input.GetKeyUp(KeyCode.Delete) || Input.GetKeyUp(KeyCode.Escape)) {
                curFurnit2D.Destroy();
                DropCatalogItem();
            }
            else if (Input.GetMouseButtonUp(1) && !EventSystem.current.IsPointerOverGameObject(-1)) {
                curFurnit2D.Destroy();
                DropCatalogItem();
            }
        }
    }

    private void OnEnable() {
        CatalogItem.OnItemTaken += OnCatalogItemTaken;
    }

    private void OnDisable() {
        CatalogItem.OnItemTaken -= OnCatalogItemTaken;
    }
    #endregion
}