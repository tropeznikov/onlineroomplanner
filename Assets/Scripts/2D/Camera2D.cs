﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Camera2D : MonoBehaviour {
    public delegate void MoveAction();
    public static event MoveAction OnMoved = delegate { };

    private Camera cam;
    private GameObject plane;
    private float curAspect; // текущее соотношение сторон экрана
    private float distance = 2f; // расстояние от target до камеры
    private float zoomSpeed = 20f; //скорость зума
    private float zoom_min = 0.5f; // мин. дистанция зума
    private float zoom_max = 4f; // макс. дистанция зума
    private bool zoomBlocked = false;
    private Vector3 mousePos;

    private void ToHomePosition() {
        transform.position = new Vector3(0.5f, distance, 0f);
        transform.rotation = Quaternion.Euler(90, 0, 0);
        cam.orthographicSize = distance;
        curAspect = cam.aspect;
        OnMoved();
    }

    private void Move(float dX, float dY) {
        transform.Translate(new Vector3(-dX, -dY, 0));
        OnMoved();
    }

    private void Zoom(float zoomDist) {
        distance -= zoomDist;

        if (distance < zoom_min)
            distance = zoom_min;
        else if (distance > zoom_max)
            distance = zoom_max;

        cam.orthographicSize = distance;
        OnMoved();
    }

    private void BlockZoom() {
        zoomBlocked = true;
    }

    private void UnBlockZoom() {
        zoomBlocked = false;
    }

    private void Start() {
        cam = gameObject.GetComponent<Camera>();
        plane = GameObject.Find("Plane");
        ToHomePosition();
    }

    private void LateUpdate() {
        // Смещение камеры
        if (Input.GetMouseButtonDown(2) && !EventSystem.current.IsPointerOverGameObject(-1)) {
            mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(plane);
        }

        if (Input.GetMouseButton(2) && !EventSystem.current.IsPointerOverGameObject(-1)) {
            Vector3 mousePos2 = MouseUtils.GetMouseWorldPosAtDistToGO(plane);
            Vector3 moveVect = mousePos2 - mousePos;
            float dX = moveVect.x;
            float dY = moveVect.z;
            Move(dX, dY);
            mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(plane);
        }

        // увеличение - уменьшение		
        if (!zoomBlocked && Input.GetAxis("Mouse ScrollWheel") != 0 && !EventSystem.current.IsPointerOverGameObject(-1)) {
            float zoomDist = Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomSpeed;
            Zoom(zoomDist);
        }

        // изменение размера экрана
        if (curAspect != cam.aspect) {
            curAspect = cam.aspect;
            OnMoved();
        }
    }

    private void OnEnable() {
        Furniture2D.OnBlockScroll += BlockZoom;
        Furniture2D.OnUnBlockScroll += UnBlockZoom;
    }

    private void OnDisable() {
        Furniture2D.OnBlockScroll -= BlockZoom;
        Furniture2D.OnUnBlockScroll -= UnBlockZoom;
    }
}
