﻿using UnityEngine;

public class Data2D : Singleton<Data2D> {
    private Vector3 normal = Vector3.up;

    private float roomPointsPosY = 0.051f;
    private float pointRadius = 0.1f;
    private float pointHeight = 0.004f;
    private float wallThickness = 0.15f;
    private float planLinesThickness = 0.007f;
    private float dimLinesThickness = 0.005f;

    private float wallsHeight = 2.7f;
    private float defaultWindowHeight = 1.4f;
    private float defaultWindowDistOverFloor = 0.7f;
    private float defaultWindowWidth = 1.6f;
    private float defaultDoorHeight = 2.1f;
    private float defaultDoorWidth = 0.8f;

    private string mainUrl = "http://localhost/onlineroomplanner/";

    private bool snappingOn = true;
    private bool doorInsertOn = false;
    private bool windowInsertOn = false;
    private bool objectInsertOn = false;
    private bool angleDimShowOn = true;
    private bool wallDimShowOn = true;

    #region свойства
    public Vector3 Normal {
        get { return normal; }
    }

    public float RoomPointsPosY {
        get { return roomPointsPosY; }
    }

    public float PointRadius {
        get { return pointRadius; }
    }

    public float PointHeight {
        get { return pointHeight; }
    }

    public float WallThickness {
        get { return wallThickness; }
    }

    public float PlanLinesThickness {
        get { return planLinesThickness; }
    }

    public float DimLinesThickness {
        get { return dimLinesThickness; }
    }

    public float WallsHeight {
        get { return wallsHeight; }
        set { wallsHeight = value; }
    }

    public float DefaultWindowHeight {
        get { return defaultWindowHeight; }
    }

    public float DefaultWindowDistOverFloor {
        get { return defaultWindowDistOverFloor; }
    }

    public float DefaultWindowWidth {
        get { return defaultWindowWidth; }
    }

    public float DefaultDoorHeight {
        get { return defaultDoorHeight; }
    }

    public float DefaultDoorWidth {
        get { return defaultDoorWidth; }
    }

    public string MainUrl {
        get { return mainUrl; }
    }

    public bool SnappingOn {
        get { return snappingOn; }
        set { snappingOn = value; }
    }

    public bool DoorInsertOn {
        get { return doorInsertOn; }
        set {
            if (value) {
                CursorImage.Instance.SetImage(23, 48,
                    ResHolder.Instance.DoorIconGrey, 10, 0);
                CursorImage.Instance.Show();
            }
            else if (!windowInsertOn) {
                CursorImage.Instance.Hide();
            }
            doorInsertOn = value;
        }
    }

    public bool WindowInsertOn {
        get { return windowInsertOn; }
        set {
            if (value) {
                CursorImage.Instance.SetImage(48, 48,
                    ResHolder.Instance.WindowIconGrey, 10, 0);
                CursorImage.Instance.Show();
            }
            else if (!doorInsertOn) {
                CursorImage.Instance.Hide();
            }
            windowInsertOn = value;
        }
    }

    public bool ObjectInsertOn {
        get { return objectInsertOn; }
        set { objectInsertOn = value; }
    }

    public bool AngleDimShowOn {
        get { return angleDimShowOn; }
        set { angleDimShowOn = value; }
    }
    public bool WallDimShowOn {
        get { return wallDimShowOn; }
        set { wallDimShowOn = value; }
    }
    #endregion

    public bool SetWallsHeight(float value) {
        if (value < 2f || value > 5f) {
            ModalPanel.Instance.Warning("Высота стен должна быть от 200 до 500 см");
            return false;
        }
        else {
            wallsHeight = value;
            return true;
        }
    }
}