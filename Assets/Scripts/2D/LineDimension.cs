﻿using UnityEngine;

public class LineDimension : MonoBehaviour {

    private int elemsCount;
    private float dist = 0.3f;
    private float distToText = 0.15f;
    private float sideLength = 0.15f;
    private LineRenderer[] lines;
    private DimensionText[] texts;
    private string measure = "";
    private bool visible = true;

    public bool Visible {
        get { return visible; }
        set {
            visible = value;
            foreach (LineRenderer line in lines)
                line.gameObject.SetActive(value);
            foreach (DimensionText text in texts)
                text.Visible = value;
        }
    }

    public void Init(int count, bool showMeasures) {
        elemsCount = count;
        if (showMeasures)
            measure = " см";
        InitLines(elemsCount + 2);
        InitTexts(elemsCount);
    }

    public void Redraw(params Vector3[] points) {
        if (points.Length != elemsCount + 1) {
            Debug.Log("Linedimension Redraw: неверное число аргументов: " + points.Length);
            return;
        }

        Vector3 startPoint, endPoint, heightVect, normVect, normPerp;
        heightVect = new Vector3(0f, 0.004f, 0f);
        normVect = (points[points.Length - 1] - points[0]);
        normVect /= normVect.magnitude;
        normPerp = Vector3.Cross(-1 * RoomData.Instance.Normal, normVect);
        normPerp /= normPerp.magnitude;

        for (int i = 0; i < points.Length; i++) {
            // чертим засечки
            startPoint = points[i] + normPerp * (dist - sideLength / 2) + heightVect;
            endPoint = points[i] + normPerp * (dist + sideLength / 2) + heightVect;
            lines[i].DrawLine(startPoint, endPoint);

            // пишем размеры
            if (i < texts.Length) {
                float length = (points[i + 1] - points[i]).magnitude;
                texts[i].Position = points[i] + normVect * length / 2 + normPerp * (dist + distToText);
                texts[i].text = (length * 100).ToString("f0") + measure;
            }
        }
        // чертим основную линию
        startPoint = points[0] + normPerp * dist + heightVect;
        endPoint = points[points.Length - 1] + normPerp * dist + heightVect;
        lines[lines.Length - 1].DrawLine(startPoint, endPoint);
    }

    public void Destroy() {
        for (int i = 0; i < texts.Length; i++) {
            texts[i].Destroy();
        }
        for (int i = 0; i < lines.Length; i++) {
            Destroy(lines[i].gameObject);
        }
    }

    private void InitLines(int count) {
        GameObject[] lineGOs = new GameObject[count];
        lines = new LineRenderer[count];
        for (int i = 0; i < count; i++) {
            lineGOs[i] = new GameObject("LineDimSegment" + i);
            lineGOs[i].transform.parent = gameObject.transform;
            lines[i] = lineGOs[i].AddComponent<LineRenderer>();
            lines[i].material = ResHolder.Instance.BlackLineMaterial;
            float thickness = Data2D.Instance.DimLinesThickness;
            lines[i].SetWidth(thickness, thickness);
        }
    }

    private void InitTexts(int count) {
        GameObject[] textGOs = new GameObject[count];
        texts = new DimensionText[count];
        for (int i = 0; i < count; i++) {
            textGOs[i] = new GameObject("LineDimText" + i);
            texts[i] = textGOs[i].AddComponent<DimensionText>();
            texts[i].Init();
        }
    }
}
