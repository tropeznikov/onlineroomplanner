﻿using UnityEngine;
using System.Collections.Generic;

public class Point2D : MonoBehaviour {
    AngleDimension dimension;

    #region свойства
    public AngleDimension Dimension {
        get { return dimension; }
    }

    // номер точки
    public int Number {
        get { return Room.Points.IndexOf(this); }
    }

    // координаты точки
    public Vector3 Position {
        get { return transform.position; }
    }

    // контур помещения, в котором находится точка
    public Room2D Room { get; set; }

    // координаты "внешней" точки стены, противоположной данной точке
    // используется для генерации меша стены
    public Vector3 OuterPosition { get; private set; }
    #endregion

    #region методы
    public void Init(Room2D room, Vector3 pos) {
        Room = room;
        gameObject.name = "point " + Room.Points.Count;
        gameObject.transform.position = pos;
        gameObject.transform.parent = room.gameObject.transform;
        float pointRadius = Data2D.Instance.PointRadius;
        float pointHeight = Data2D.Instance.PointHeight;
        gameObject.transform.localScale = new Vector3(pointRadius * 2, pointHeight, pointRadius * 2);
        gameObject.GetComponent<MeshRenderer>().material = ResHolder.Instance.PointMaterial;
        dimension = gameObject.AddComponent<AngleDimension>();
        gameObject.layer = 9;
        Room.Points.Add(this);
    }

    // получение списка соседних точек
    public List<Point2D> GetNeighborPoints() {
        List<Point2D> points = new List<Point2D>();
        int prevPtIdx;
        int netxPtIdx;

        if (Room.ShapeClosed) {
            prevPtIdx = (Room.Points.Count + Number - 1) % Room.Points.Count;
            netxPtIdx = (Number + 1) % Room.Points.Count;
            points.Add(Room.Points[prevPtIdx]);
            points.Add(Room.Points[netxPtIdx]);
        }
        else {
            prevPtIdx = Number - 1;
            netxPtIdx = Number + 1;
            if (prevPtIdx >= 0 && prevPtIdx < Room.Points.Count)
                points.Add(Room.Points[prevPtIdx]);
            if (netxPtIdx >= 0 && netxPtIdx < Room.Points.Count)
                points.Add(Room.Points[netxPtIdx]);
        }
        return points;
    }

    // получение списка соседних стен
    public List<Wall2D> GetNeighborWalls() {
        List<Wall2D> walls = new List<Wall2D>();
        int prevWallIdx;
        int nextWallIdx;

        if (Room.ShapeClosed) {
            prevWallIdx = (Room.Walls.Count + Number - 1) % Room.Walls.Count;
            nextWallIdx = Number;
            walls.Add(Room.Walls[prevWallIdx]);
            walls.Add(Room.Walls[nextWallIdx]);
        }
        else {
            prevWallIdx = Number - 1;
            nextWallIdx = Number;
            if (prevWallIdx >= 0 && prevWallIdx < Room.Walls.Count)
                walls.Add(Room.Walls[prevWallIdx]);
            if (nextWallIdx >= 0 && nextWallIdx < Room.Walls.Count)
                walls.Add(Room.Walls[nextWallIdx]);
        }

        return walls;
    }

    // нахождение угла между двумя стенами смежными c точкой
    // если у точки 2 смежные стены, то возвращает true и записывает в angle значение угла
    // иначе возвращает false и записывает в angle ноль
    public bool GetAngleBetweenNeighborWalls(out float angle) {
        List<Wall2D> walls = GetNeighborWalls();

        if (walls.Count == 2) {
            Wall2D leftWall = walls[0];
            Wall2D rightWall = walls[1];
            Vector3 p0 = leftWall.StartPoint.Position;
            Vector3 p1 = Position;
            Vector3 p2 = rightWall.EndPoint.Position;
            Vector3 v1 = p0 - p1;
            Vector3 v2 = p2 - p1;
            angle = Math3DFunc.Angle360BetweenVectors(v1, v2, RoomData.Instance.Normal);
            return true;
        }
        else {
            angle = 0f;
            return false;
        }
    }

    // изменить координаты "внешней" точки стены
    public void UpdateOuterPosition() {
        float width = Data2D.Instance.WallThickness;
        List<Wall2D> walls = GetNeighborWalls();
        Wall2D wall = walls[0];
        Vector3 perp = wall.NormPerp * width;
        OuterPosition = Position + perp;

        if (walls.Count == 2) {
            int dir = 1;
            float angle;
            if (GetAngleBetweenNeighborWalls(out angle)) {
                if (angle > 180)
                    dir *= -1;
                angle = Mathf.Abs(180 - angle) / 2;
                float cathLength = Mathf.Tan(angle * Mathf.Deg2Rad) * width;
                OuterPosition += wall.NormVect * dir * cathLength;
            }
        }
    }

    private bool Move(Vector3 mousePos) {
        // выравниваем координаты точки относительно соседних с ней точек
        Vector3 newPos = SnapPos(mousePos);
        newPos.y = Data2D.Instance.RoomPointsPosY;

        Vector3 curPos = transform.position;
        transform.position = newPos;

        foreach (Wall2D wall in GetNeighborWalls()) {
            // проверка на пересечение точек стены и окон/дверей
            if (wall.DetectCollisions(this, Position)) {
                transform.position = curPos;
                return false;
            }
        }

        List<Point2D> points = GetNeighborPoints();
        points.Add(this);

        // проверка на допустимое значение угла между стенами
        float angle;
        foreach (Point2D point in points) {
            if (point.GetAngleBetweenNeighborWalls(out angle)) {
                if (angle < 30 || angle > 330) {
                    transform.position = curPos;
                    return false;
                }
            }
        }

        Room.RedrawShape(this);

        foreach (Point2D point in points)
            point.Dimension.Redraw();
        return true;
    }

    public void Destroy() {
        Room.Points.Remove(this);
        dimension.Destroy();
        Destroy(gameObject);
    }

    // выравнивние координат точки относительно её соседних с ней точек
    // позволяет выравнивать углы между стеными      
    private Vector3 SnapPos(Vector3 mousePos) {
        float snappingDist = 0.1f;
        Vector3 newPos = mousePos;
        if (Data2D.Instance.SnappingOn) {
            List<Point2D> neighborPoints = GetNeighborPoints();

            // если разница между x/z координатой точки и соотв. координатой соседней точки < snappingDist,
            // то x/z координата точки становится равной соотв. координате соседней точки
            foreach (Point2D pt in neighborPoints) {
                if (Mathf.Abs(mousePos.x - pt.Position.x) < snappingDist) {
                    newPos.x = pt.Position.x;
                }
                if (Mathf.Abs(mousePos.z - pt.Position.z) < snappingDist) {
                    newPos.z = pt.Position.z;
                }
            }
        }
        return newPos;
    }

    private void OnMouseDrag() {
        Vector3 mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
        Move(mousePos);
    }

    private void OnMouseEnter() {
        transform.localScale *= 1.3f;
        Color colorPicker = GetComponent<MeshRenderer>().material.color;
        colorPicker.a += 50f / 255;
        GetComponent<MeshRenderer>().material.color = colorPicker;

        if (!Data2D.Instance.AngleDimShowOn) {
            dimension.Visible = true;
        }

        if (!Data2D.Instance.WallDimShowOn) {
            foreach (Wall2D wall in GetNeighborWalls()) {
                wall.Dimension.Visible = true;
            }
        }
    }

    private void OnMouseExit() {
        transform.localScale /= 1.3f;
        Color colorPicker = GetComponent<MeshRenderer>().material.color;
        colorPicker.a -= 50f / 255;
        GetComponent<MeshRenderer>().material.color = colorPicker;

        if (!Data2D.Instance.AngleDimShowOn) {
            dimension.Visible = false;
        }

        if (!Data2D.Instance.WallDimShowOn) {
            foreach (Wall2D wall in GetNeighborWalls()) {
                wall.Dimension.Visible = false;
            }
        }
    }
    #endregion
}


