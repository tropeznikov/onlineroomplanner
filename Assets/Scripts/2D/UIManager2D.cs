﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager2D : MonoBehaviour {
    private Room2D room;
    private Text snapBtnText;
    private Text angleDimBtnText;
    private Text wallDimBtnText;
    private ParamInputPanel wallHeightInputPanel;

    public void ChangeSnappingMode() {
        Data2D.Instance.SnappingOn = !Data2D.Instance.SnappingOn;
        SetSnappingMode();
    }

    public void ChangeAngleDimShowMode() {
        Data2D.Instance.AngleDimShowOn = !Data2D.Instance.AngleDimShowOn;
        SetAngleDimShowMode();
    }

    public void ChangeWallDimShowMode() {
        Data2D.Instance.WallDimShowOn = !Data2D.Instance.WallDimShowOn;
        SetWallDimShowMode();
    }

    public void InsertObject() {
        CatalogPanel.Instance.Show();
    }

    public void InsertDoor() {
        if (!Data2D.Instance.DoorInsertOn) {
            Data2D.Instance.DoorInsertOn = true;
            Data2D.Instance.WindowInsertOn = false;
        }
        else {
            Data2D.Instance.DoorInsertOn = false;
        }
    }

    public void InsertWindow() {
        if (!Data2D.Instance.WindowInsertOn) {
            Data2D.Instance.WindowInsertOn = true;
            Data2D.Instance.DoorInsertOn = false;
        }
        else {
            Data2D.Instance.WindowInsertOn = false;
        }
    }

    public void GoTo3D() {
        float wallHeight = Data2D.Instance.WallsHeight;
        if (wallHeight < 2f || wallHeight > 5f) {
            ModalPanel.Instance.Warning("Высота стен должна быть от 2 до 5 метров");
            return;
        }
        else if (!room.ShapeClosed) {
            ModalPanel.Instance.Warning("Для перехода в 3D нужно сначала создать контур помещения");
            return;
        }
        else {
            RoomData.Instance.SaveRoomData2D(room, Data2D.Instance.WallsHeight);
            Application.LoadLevel("3D");
        }
    }

    public void ClearRoomShape() {
        ModalPanel.Instance.Question("Вы уверены, что хотите удалить контур помещения?", room.ClearShape, () => { });
    }

    private void SetSnappingMode() {
        if (Data2D.Instance.SnappingOn)
            snapBtnText.text = "Отключить выравнивание углов";
        else
            snapBtnText.text = "Включить выравнивание углов";
    }

    private void SetAngleDimShowMode() {
        if (Data2D.Instance.AngleDimShowOn) {
            room.ShowAngleDimensions();
            angleDimBtnText.text = "Не показывать размеры углов";
        }
        else {
            room.HideAngleDimensions();
            angleDimBtnText.text = "Показать размеры углов";
        }
    }

    private void SetWallDimShowMode() {
        if (Data2D.Instance.WallDimShowOn) {
            room.ShowWallDimensions();
            wallDimBtnText.text = "Не показывать размеры стен";
        }
        else {
            room.HideWallDimensions();
            wallDimBtnText.text = "Показать размеры стен";
        }
    }

    private void Start() {
        room = FindObjectOfType<Room2D>();

        snapBtnText = GameObject.Find("SnappingButton").GetComponentInChildren<Text>();
        angleDimBtnText = GameObject.Find("AngleDimShowButton").GetComponentInChildren<Text>();
        wallDimBtnText = GameObject.Find("WallDimShowButton").GetComponentInChildren<Text>();

        wallHeightInputPanel = GameObject.Find("WallsHeightInputPanel").GetComponent<ParamInputPanel>();
        wallHeightInputPanel.Init("Высота стен", Data2D.Instance.WallsHeight, "см", 1, Data2D.Instance.SetWallsHeight);

        SetSnappingMode();
        SetAngleDimShowMode();
        SetWallDimShowMode();
    }
}
