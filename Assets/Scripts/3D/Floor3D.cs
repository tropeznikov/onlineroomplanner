﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Floor3D : CoverableSurface3D {
    private Room3D room;

    public override float CoverageArea {
        get {
            if (MaterialRes != null)
                return Area;
            else
                return 0;
        }
    }

    public void Init(Room3D room3d, MaterialRes materialRes) {
        this.room = room3d;
        List<Vector3> points = new List<Vector3>();
        foreach (Wall3D wall3d in room.Walls) {
            points.Add(wall3d.StartPointPos);
        }

        base.Init(points, RoomData.Instance.Normal, MeshTexMode.WrapRepeat);

        gameObject.name = "floor";
        gameObject.transform.parent = room.gameObject.transform;
        this.materialRes = materialRes;
        LoadTexture();
    }

    protected override void LoadTexture() {
        if (materialRes != null) {
            Material material = gameObject.GetComponent<MeshRenderer>().material;
            material.mainTexture = materialRes.Texture;
            float texScale = 100 / (float)materialRes.Size;
            material.mainTextureScale = new Vector2(texScale, texScale);
        }
    }

    protected override void OnMouseUpAsButton() {
        if (!EventSystem.current.IsPointerOverGameObject(-1)) {
            base.OnMouseUpAsButton();
            if (Data3D.Instance.CatalogItemTaken) {
                UseTakenMaterial();
            }
        }
    }
}