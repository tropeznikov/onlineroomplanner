﻿using UnityEngine;

public class Controller3D : MonoBehaviour {
    private void Start() {
        Hide();
    }

    public void Show() {
        foreach (Transform child in transform)
            child.gameObject.SetActive(true);
    }

    public void Hide() {
        Furniture3D furnit3d = GetComponentInChildren<Furniture3D>();
        if (furnit3d != null)
            furnit3d.gameObject.transform.parent = null;

        foreach (Transform child in transform)
            child.gameObject.SetActive(false);
    }
}
