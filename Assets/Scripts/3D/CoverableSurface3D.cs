﻿public abstract class CoverableSurface3D : Surface3D {
    protected MaterialRes materialRes;

    public MaterialRes MaterialRes {
        get { return materialRes; }
    }

    public abstract float CoverageArea {
        get;
    }

    protected void UseTakenMaterial() {
        materialRes = Data3D.Instance.CurMaterialRes;
        LoadTexture();
    }
}