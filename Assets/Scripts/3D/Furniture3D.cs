﻿using UnityEngine;

public class Furniture3D : Furniture {
    private GameObject model;
    private GameObject controller;

    #region свойства
    public override float Length {
        get { return length; }
        protected set {
            length = value;
            float deltaLength = length / resource.DefaultLength;
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = new Vector3(deltaLength, oldScale.y, oldScale.z);
        }
    }

    public override float Width {
        get { return width; }
        protected set {
            width = value;
            float deltaWidth = width / resource.DefaultWidth;
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = new Vector3(oldScale.x, oldScale.y, deltaWidth);
        }
    }

    public override float Height {
        get { return height; }
        protected set {
            height = value;
            float deltaHeight = height / resource.DefaultHeight;
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = new Vector3(oldScale.x, deltaHeight, oldScale.z);
        }
    }

    public override float DistFromFloor {
        get { return distFromFloor; }
        protected set {
            float oldDist = distFromFloor;
            distFromFloor = value;
            gameObject.transform.position += new Vector3(0f, distFromFloor - oldDist, 0f);
        }
    }

    public GameObject Model {
        get { return resource != null ? resource.Model : ResHolder.Instance.NoModel; }
    }
    #endregion

    #region методы
    public override void Destroy() {
        HideAxes();
        base.Destroy();
    }

    public override bool Init(Room room, FurnitureData data) {
        if (base.Init(room, data)) {
            gameObject.transform.position += new Vector3(0f, Data2D.Instance.RoomPointsPosY, 0f);
            model = Instantiate(resource.Model);
            model.transform.parent = gameObject.transform;
            model.transform.localPosition = Vector3.zero;
            model.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            model.transform.localScale = Vector3.one;
            model.AddComponent<Model3D>();
            model.tag = "3Dmodel";
            controller = GameObject.Find("Controller");
            return true;
        }
        else {
            Destroy();
            return false;
        }
    }

    public override void OnMouseUpAsButton() {
        if (!Data3D.Instance.CatalogItemTaken)
            base.OnMouseUpAsButton();
    }

    protected override void Select() {
        if (!isSelected) {
            base.Select();
            ShowAxes();
        }
    }

    protected override void Deselect() {
        if (isSelected) {
            base.Deselect();
            HideAxes();
        }
    }

    private void ShowAxes() {
        controller.transform.position = transform.position;
        controller.transform.position += new Vector3(0, Height, 0);
        controller.transform.rotation = transform.rotation;
        transform.parent = controller.transform;
        controller.GetComponent<Controller3D>().Show();
    }

    private void HideAxes() {
        controller.GetComponent<Controller3D>().Hide();
        distFromFloor = transform.position.y - Data2D.Instance.RoomPointsPosY;
    }
    #endregion
}