﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class UIManager3D : MonoBehaviour {
    private Room3D room;
    private Controller3D controller;

    public void GoTo2D() {
        Data3D.Instance.CatalogItemTaken = false;
        RoomData.Instance.SaveRoomData3D(room);
        Application.LoadLevel("2D");
    }

    public void OnReportBtnClick() {
        StartCoroutine(ShowReport());
    }

    private IEnumerator ShowReport() {
        int hash = DateTime.Now.GetHashCode();

        CoverableSurface3D[] surfaces = new CoverableSurface3D[room.Walls.Length + 1];
        for (int i = 0; i < room.Walls.Length; i++)
            surfaces[i] = room.Walls[i];
        surfaces[surfaces.Length - 1] = room.Floor;

        yield return StartCoroutine(UploadScreenshots(room, surfaces, hash));

        yield return StartCoroutine(CreateReport(room, surfaces, hash));

        string url = Data2D.Instance.MainUrl + "reports/" + hash + "_report.html";
        ModalPanel.Instance.OpenUrlWindow("Развертка и смета готовы. Сейчас они будут " +
            "открыты в новом окне браузера.", url);
    }

    private IEnumerator UploadScreenshots(Room3D room, CoverableSurface3D[] surfaces, int hash) {
        controller.Hide();
        foreach (var furnit in room.Furniture)
            furnit.gameObject.SetActive(false);

        Camera camera = GameObject.Find("SecondaryCamera").GetComponent<Camera>();
        camera.depth = 2; // делаем данную камеру текущей

        for (int i = 0; i < surfaces.Length; i++) {
            Bounds bounds = surfaces[i].GetComponent<MeshFilter>().mesh.bounds;
            Vector3 center = bounds.center;
            Vector3 pos = center + surfaces[i].Normal;
            camera.transform.position = pos;
            camera.transform.LookAt(center);

            Vector3 screenPoint1, screenPoint2;
            if (i != surfaces.Length - 1) {
                screenPoint1 = camera.WorldToScreenPoint(surfaces[i].Points[0]);
                screenPoint2 = camera.WorldToScreenPoint(surfaces[i].Points[2]);
            }
            else {
                screenPoint1 = camera.WorldToScreenPoint(bounds.center - bounds.extents);
                screenPoint2 = camera.WorldToScreenPoint(bounds.center + bounds.extents);
            }

            int startX = (int)screenPoint1.x;
            int startY = (int)screenPoint1.y;
            int width = (int)Mathf.Abs(screenPoint1.x - screenPoint2.x);
            int height = (int)Mathf.Abs(screenPoint1.y - screenPoint2.y);

            //Делать снимок экрана нужно только после того, как завершится рендеринг кадра
            yield return new WaitForEndOfFrame();

            // создаем текстуру заданного размера в формате RGB24
            var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            // читаем в текстуру содержимое экрана, вхожящее в заданный прямоугольник
            tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
            tex.Apply();

            // Кодируем текстуру в PNG
            byte[] bytes = tex.EncodeToPNG();
            Destroy(tex);

            // Создаем Web форму
            WWWForm form = new WWWForm();
            form.AddBinaryData("fileUpload", bytes, hash + "_image" + i + ".png", "image/png");

            // отправляем форму
            string url = Data2D.Instance.MainUrl + "save_file.php";
            WWW w = new WWW(url, form);
            yield return w;
            if (!string.IsNullOrEmpty(w.error))
                print(w.error);
        }
        camera.depth = 0; // делаем текущей основную камеру

        foreach (var furnit in room.Furniture)
            furnit.gameObject.SetActive(true);
    }

    private IEnumerator CreateReport(Room3D room, CoverableSurface3D[] surfaces, int hash) {
        WWWForm form = new WWWForm();
        form.AddField("hash", hash);
        for (int i = 0; i < room.Walls.Length; i++) {
            form.AddField("walls_img[]", hash + "_image" + i + ".png");
            form.AddField("walls_area[]", room.Walls[i].Area.ToString("f1"));
        }
        form.AddField("floor_img", hash + "_image" + room.Walls.Length + ".png");
        form.AddField("floor_area", room.Floor.Area.ToString("f1"));

        JSONObject j;

        var furnitCount = room.Furniture.GroupBy(f => f.Id)
            .Select(g => new { Name = g.Key, Count = g.Count() });
        foreach (var group in furnitCount) {
            j = new JSONObject(JSONObject.Type.OBJECT);
            j.AddField("id", group.Name);
            j.AddField("count", group.Count);
            form.AddField("obj_json[]", j.Print());
        }

        var matAreaSum = surfaces.Where(s => s.MaterialRes != null)
            .GroupBy(s => s.MaterialRes.Id)
            .Select(g => new { Id = g.Key, TotalArea = g.Sum(s => s.CoverageArea) });
        foreach (var group in matAreaSum) {
            j = new JSONObject(JSONObject.Type.OBJECT);
            j.AddField("id", group.Id);
            j.AddField("area", group.TotalArea.ToString("f1"));
            form.AddField("mat_json[]", j.Print());
        }

        string url = Data2D.Instance.MainUrl + "create_report.php";
        WWW www = new WWW(url, form);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            Debug.Log(www.error);
    }

    private void OnCatalogItemTaken(CatalogItem item) {
        Data3D.Instance.CatalogItemTaken = true;
        Data3D.Instance.CurMaterialRes = item as MaterialRes;
        CursorImage.Instance.SetImage(64, 64, Data3D.Instance.CurMaterialRes.Image, 12, 4);
        CursorImage.Instance.Show();
    }

    private void Start() {
        room = FindObjectOfType<Room3D>();
        controller = FindObjectOfType<Controller3D>();
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape) || Input.GetMouseButtonUp(1)) {
            Data3D.Instance.CatalogItemTaken = false;
            CursorImage.Instance.Hide();
        }
    }

    private void OnEnable() {
        CatalogItem.OnItemTaken += OnCatalogItemTaken;
    }

    private void OnDisable() {
        CatalogItem.OnItemTaken -= OnCatalogItemTaken;
    }
}