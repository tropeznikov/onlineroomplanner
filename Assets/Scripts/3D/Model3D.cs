﻿using UnityEngine;
using System.Collections;

public class Model3D : MonoBehaviour {
    private Furniture3D furnit3d;

    public void OnMouseUpAsButton() {
        if (furnit3d)
            furnit3d.OnMouseUpAsButton();
    }

    public void OnMouseOver() {
        if (furnit3d)
            furnit3d.OnMouseOver();
    }

    private void Start() {
        Transform parent = transform.parent;
        if (parent)
            furnit3d = parent.gameObject.GetComponent<Furniture3D>();
    }
}