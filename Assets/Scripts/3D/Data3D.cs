﻿public class Data3D : Singleton<Data3D> {
    private bool catalogItemTaken = false;

    public MaterialRes CurMaterialRes {
        get; set;
    }

    public bool CatalogItemTaken {
        get { return catalogItemTaken; }
        set { catalogItemTaken = value; }
    }
}