﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Wall3D : CoverableSurface3D {
    public delegate void UseMaterialOnAllWallsAction();
    public static event UseMaterialOnAllWallsAction OnUseMaterialOnAllWalls = delegate { };

    private Room3D room;
    private int number;
    private Door3D[] doors;
    private Window3D[] windows;

    #region свйоства
    public Room3D Room {
        get { return room; }
    }

    public int Number {
        get { return number; }
    }

    public Vector3 StartPointPos {
        get { return Points[0]; }
    }

    public Vector3 EndPointPos {
        get { return Points[3]; }
    }

    public Door3D[] Doors {
        get { return doors; }
    }

    public Window3D[] Windows {
        get { return windows; }
    }

    public override float CoverageArea {
        get {
            if (MaterialRes != null) {
                float windowsArea = 0;
                float doorsArea = 0;
                foreach (Window3D window in windows)
                    windowsArea += window.Area;
                foreach (Door3D door in doors)
                    doorsArea += door.Area;
                return Area - windowsArea - doorsArea;
            }
            else
                return 0;
        }
    }
    #endregion

    #region методы
    public void Init(Room3D room3d, int number, WallData wallData) {
        Vector3 startPointPos = wallData.StartPointPos;
        Vector3 endPointPos = wallData.EndPointPos;
        Vector3 tmpNormal = Vector3.Cross(RoomData.Instance.Normal, endPointPos - startPointPos);
        Vector3 normal = tmpNormal / tmpNormal.magnitude;
        Vector3 heightVect = RoomData.Instance.WallsHeight * RoomData.Instance.Normal;

        List<Vector3> points = new List<Vector3>();
        points.Add(startPointPos);
        points.Add(startPointPos + heightVect);
        points.Add(endPointPos + heightVect);
        points.Add(endPointPos);

        base.Init(points, normal, MeshTexMode.WrapRepeat);

        room = room3d;
        this.number = number;

        gameObject.name = "wall" + number;
        gameObject.transform.parent = room.gameObject.transform;
        materialRes = wallData.MaterialRes;
        LoadTexture();

        doors = new Door3D[wallData.Doors.Count];
        for (int i = 0; i < doors.Length; i++) {
            GameObject doorGO = new GameObject();
            Door3D door3d = doorGO.AddComponent<Door3D>();
            door3d.Init(this, i, wallData.Doors[i]);
        }

        windows = new Window3D[wallData.Windows.Count];
        for (int i = 0; i < windows.Length; i++) {
            GameObject windowGO = new GameObject();
            Window3D window3d = windowGO.AddComponent<Window3D>();
            window3d.Init(this, i, wallData.Windows[i]);
        }

        room.Walls[number] = this;
    }

    protected override void LoadTexture() {
        if (materialRes != null) {
            Material material = gameObject.GetComponent<MeshRenderer>().material;
            material.mainTexture = materialRes.Texture;
            float texScale = 100 / (float)materialRes.Size;
            material.mainTextureScale = new Vector2(texScale, texScale);
        }
    }

    protected override void OnMouseUpAsButton() {
        if (!EventSystem.current.IsPointerOverGameObject(-1)) {
            base.OnMouseUpAsButton();
            if (Data3D.Instance.CatalogItemTaken) {
                ModalPanel.Instance.Question("Использовать данный материал на всех стенах?",
                    UseTakenMaterialOnAllWalls, UseTakenMaterial, () => { });
            }
        }
    }

    private void UseTakenMaterialOnAllWalls() {
        OnUseMaterialOnAllWalls();
    }

    private void OnEnable() {
        OnUseMaterialOnAllWalls += UseTakenMaterial;
    }

    private void OnDisable() {
        OnUseMaterialOnAllWalls -= UseTakenMaterial;
    }
    #endregion
}