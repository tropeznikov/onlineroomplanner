﻿using UnityEngine;

public class RotationTorus : Dragable3D {
    private float rotationSpeed = 50f;

    protected override void OnDrag() {
        float angle = -1 * Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
        transform.parent.Rotate(new Vector3(0, angle, 0));
    }
}