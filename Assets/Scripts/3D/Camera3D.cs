﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Camera3D : MonoBehaviour {

    public Transform target; //точка, вокруг которой крутится камера, в качестве неё скрипту должен передаваться
                             //пустой GameObject, расположенный в центре помещения на высоте 1.5 от пола						
    public float distance = 5f; // расстояние от target до камеры
    public float xRotateSpeed = 250f; // скорость поворота камеры по оси Х
    public float yRotateSpeed = 120f; // скорость поворота камеры по оси Y
    public float yRotateMinLimit = -45; // мин. угол поворота камеры по оси Х
    public float yRotateMaxLimit = 80f; // макс. угол поворота камеры по оси Y
    public float xMoveSpeed = 10f; // скорость смещения камеры по оси Х
    public float yMoveSpeed = 10f; // скорость смещения камеры по оси Y
    public float zoomSpeed = 60f; //скорость зума
    public float zoom_min = 1f; // мин. дистанция зума
    public float zoom_max = 10f; // макс. дистанция зума
    private float x;
    private float y;
    private bool rotationBlocked = false;

    private static float ClampAngle(float angle, float min, float max) {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    private void Start() {
        x = transform.eulerAngles.y;
        y = transform.eulerAngles.x;
        // изменяем положение камеры	
        transform.position = transform.rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
        //поворачиваем target по оси Y в сторону камеры	
        target.LookAt(transform);
        target.rotation = Quaternion.Euler(0, target.eulerAngles.y, 0);
    }

    private void LateUpdate() {
        // Поворот камеры
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject(-1) && !rotationBlocked) {
            x += Input.GetAxis("Mouse X") * xRotateSpeed * Time.deltaTime;
            y -= Input.GetAxis("Mouse Y") * yRotateSpeed * Time.deltaTime;
            y = ClampAngle(y, yRotateMinLimit, yRotateMaxLimit);
            transform.rotation = Quaternion.Euler(y, x, 0);
        }

        // Смещение камеры
        if (Input.GetMouseButton(2) && !EventSystem.current.IsPointerOverGameObject(-1)) {
            target.Translate(new Vector3(Input.GetAxis("Mouse X") * Time.deltaTime * xMoveSpeed,
                                                    -Input.GetAxis("Mouse Y") * Time.deltaTime * yMoveSpeed, 0.0f));

            float targetX = Mathf.Clamp(target.transform.position.x, -7f, 7f);
            float targetY = Mathf.Clamp(target.transform.position.y, -2f, 3.5f);
            float targetZ = Mathf.Clamp(target.transform.position.z, -7f, 7f);
            target.transform.position = new Vector3(targetX, targetY, targetZ);
        }

        // увеличение - уменьшение		
        if (Input.GetAxis("Mouse ScrollWheel") != 0 && !EventSystem.current.IsPointerOverGameObject(-1)) {
            distance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomSpeed;

            if (distance < zoom_min)
                distance = zoom_min;
            else if (distance > zoom_max)
                distance = zoom_max;
        }

        // изменяем положение камеры		
        transform.position = transform.rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
        target.LookAt(transform);
        target.rotation = Quaternion.Euler(0, target.eulerAngles.y, 0);
    }

    public void OnEnable() {
        Dragable3D.OnDragStart += BlockRotation;
        Dragable3D.OnDragEnd += UnBlockRotation;
    }

    public void OnDisable() {
        Dragable3D.OnDragStart -= BlockRotation;
        Dragable3D.OnDragEnd -= UnBlockRotation;
    }

    private void BlockRotation() {
        rotationBlocked = true;
    }

    private void UnBlockRotation() {
        rotationBlocked = false;
    }
    //TODO: убрать лимиты зума камеры, сделать чтобы камера при зуме "упиралась" в стену
}
