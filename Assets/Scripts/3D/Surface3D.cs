﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Surface3D : MonoBehaviour {
    private List<Vector3> points;
    private Vector3 normal;
    private Mesh mesh;

    public delegate void SelectAction(MonoBehaviour sender);
    public static event SelectAction OnSelected = delegate { };

    public List<Vector3> Points {
        get { return points; }
    }

    public Vector3 Normal {
        get { return normal; }
    }

    public float Area {
        get { return Math3DFunc.MeshArea(mesh); }
    }

    public void Init(List<Vector3> points, Vector3 normal, MeshTexMode texMode) {
        this.points = points;
        this.normal = normal;

        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        mesh = GenerateMesh(texMode);
        gameObject.GetComponent<MeshFilter>().mesh = mesh;
        gameObject.GetComponent<MeshRenderer>().material = ResHolder.Instance.WallMaterial;
        gameObject.AddComponent<MeshCollider>();
    }

    protected abstract void LoadTexture();

    private Mesh GenerateMesh(MeshTexMode texMode) {
        return MeshGenerator.GenerateMesh(points, true, normal, texMode);
    }

    protected virtual void OnMouseUpAsButton() {
        OnSelected(this);
    }
}
