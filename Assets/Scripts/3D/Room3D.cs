﻿using UnityEngine;
using System.Collections.Generic;

public class Room3D : Room {
    private Wall3D[] walls;
    private Floor3D floor;

    public Wall3D[] Walls {
        get { return walls; }
    }

    public Floor3D Floor {
        get { return floor; }
    }

    void Start() {
        WallData[] wallsData = RoomData.Instance.Walls;

        walls = new Wall3D[wallsData.Length];
        for (int i = 0; i < wallsData.Length; i++) {
            GameObject wallGO = new GameObject();
            Wall3D wall3d = wallGO.AddComponent<Wall3D>();
            wall3d.Init(this, i, wallsData[i]);
        }

        GameObject floorGO = new GameObject();
        floor = floorGO.AddComponent<Floor3D>();
        floor.Init(this, RoomData.Instance.FloorMaterialRes);

        furniture = new List<Furniture>();
        foreach (FurnitureData furnitData in RoomData.Instance.Furniture) {
            GameObject furnitGO = new GameObject();
            Furniture3D furnit3d = furnitGO.AddComponent<Furniture3D>();
            furnit3d.Init(this, furnitData);
        }

        Light pointLight = FindObjectOfType<Light>();
        Vector3 roomCenter = floor.GetComponent<MeshFilter>().mesh.bounds.center;
        pointLight.transform.position = new Vector3(roomCenter.x, 3.5f, roomCenter.z);
    }
}