﻿using UnityEngine;
using System.Collections.Generic;

public class Door3D : Surface3D {
    private Wall3D wall;
    private int number;

    public Wall3D Wall {
        get { return wall; }
    }

    public int Number {
        get { return number; }
    }

    public void Init(Wall3D wall3d, int number, DoorData doorData) {
        Vector3 startPointPos = doorData.StartPointPos;
        Vector3 endPointPos = doorData.EndPointPos;
        Vector3 normal = wall3d.Normal;
        Vector3 heightVect = doorData.Height * RoomData.Instance.Normal;
        Vector3 thicknessVect = 0.01f * normal;

        List<Vector3> points = new List<Vector3>();
        points.Add(startPointPos + thicknessVect);
        points.Add(startPointPos + thicknessVect + heightVect);
        points.Add(endPointPos + thicknessVect + heightVect);
        points.Add(endPointPos + thicknessVect);

        base.Init(points, normal, MeshTexMode.WrapClamp);

        wall = wall3d;
        this.number = number;

        gameObject.name = "door" + number;
        gameObject.transform.parent = wall.gameObject.transform;
        LoadTexture();

        wall.Doors[number] = this;
    }

    protected override void LoadTexture() {
        Material material = gameObject.GetComponent<MeshRenderer>().material;
        material.mainTexture = ResHolder.Instance.DoorTextures[0];
    }
}