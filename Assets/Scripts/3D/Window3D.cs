﻿using UnityEngine;
using System.Collections.Generic;

public class Window3D : Surface3D {
    private Wall3D wall;
    private int number;

    public Wall3D Wall {
        get { return wall; }
    }

    public int Number {
        get { return number; }
    }

    public void Init(Wall3D wall3d, int number, WindowData windowData) {
        Vector3 startPointPos = windowData.StartPointPos;
        Vector3 endPointPos = windowData.EndPointPos;
        Vector3 normal = wall3d.Normal;
        Vector3 heightVect = windowData.Height * RoomData.Instance.Normal;
        Vector3 thicknessVect = 0.01f * normal;
        Vector3 distVect = windowData.DistOverFloor * RoomData.Instance.Normal;

        List<Vector3> points = new List<Vector3>();
        points.Add(startPointPos + thicknessVect + distVect);
        points.Add(startPointPos + thicknessVect + distVect + heightVect);
        points.Add(endPointPos + thicknessVect + distVect + heightVect);
        points.Add(endPointPos + thicknessVect + distVect);

        base.Init(points, normal, MeshTexMode.WrapClamp);

        wall = wall3d;
        this.number = number;

        gameObject.name = "window" + number;
        gameObject.transform.parent = wall.gameObject.transform;
        LoadTexture();

        wall.Windows[number] = this;
    }

    protected override void LoadTexture() {
        Material material = gameObject.GetComponent<MeshRenderer>().material;
        material.mainTexture = ResHolder.Instance.WindowTextures[0];
    }
}