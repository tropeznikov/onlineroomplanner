﻿using UnityEngine;

public class AxisArrow : Dragable3D {
    public Vector3 axis;
    private Vector3 curAxis;

    protected override void OnDrag() {
        Vector3 mousePos2 = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
        Vector3 moveVect = mousePos2 - mousePos;
        curAxis = Quaternion.Euler(0f, transform.parent.rotation.eulerAngles.y, 0f) * axis;
        moveVect = Math3DFunc.ProjectPointOnLine(Vector3.zero, curAxis, moveVect);
        gameObject.transform.parent.position += moveVect;

        float posX = gameObject.transform.parent.position.x;
        float posY = Mathf.Clamp(gameObject.transform.parent.position.y, -3f, 5f);
        float posZ = gameObject.transform.parent.position.z;
        gameObject.transform.parent.position = new Vector3(posX, posY, posZ);

        mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
    }
}
