﻿using UnityEngine;

public abstract class Dragable3D : MonoBehaviour {
    protected Vector3 mousePos;
    private Material material;
    private Color defaultColor;
    private bool isDraged;
    private bool isHilighted;
    private static bool blockHilighting;

    public delegate void DragAction();
    public static event DragAction OnDragStart = delegate { };
    public static event DragAction OnDragEnd = delegate { };

    protected abstract void OnDrag();

    private void Start() {
        material = gameObject.GetComponent<MeshRenderer>().material;
        Color colorPicker = material.color;
        colorPicker.a = 120f / 255;
        material.color = colorPicker;
        defaultColor = material.color;
        isDraged = false;
        isHilighted = false;
        blockHilighting = false;
    }

    private void OnMouseOver() {
        Highlight();
    }

    private void OnMouseExit() {
        if (!isDraged)
            Dehighlight();
    }

    private void OnMouseDown() {
        isDraged = true;
        OnDragStart();
        mousePos = MouseUtils.GetMouseWorldPosAtDistToGO(gameObject);
    }

    private void OnMouseDrag() {
        OnDrag();
    }

    private void OnMouseUp() {
        isDraged = false;
        Dehighlight();
        OnDragEnd();
    }

    private void Highlight() {
        if (!blockHilighting) {
            blockHilighting = true;
            isHilighted = true;
            Color colorPicker = Color.yellow;
            colorPicker.a = defaultColor.a;
            material.color = colorPicker;
        }
    }

    private void Dehighlight() {
        if (isHilighted) {
            blockHilighting = false;
            isHilighted = false;
            material.color = defaultColor;
        }
    }
}
