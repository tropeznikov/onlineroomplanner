﻿using UnityEngine;
using UnityEngine.UI;

public class CatalogItemButton : Button {
    private Text text;
    private Image picture;
    private CatalogItem item;

    public void Init(CatalogItem item) {
        text = GetComponentInChildren<Text>();
        picture = GetComponentsInChildren<Image>()[1];

        if (text == null || picture == null || item == null)
            return;

        text.text = item.Name;
        Rect rec = new Rect(0, 0, item.Image.width, item.Image.height);
        picture.sprite = Sprite.Create(item.Image, rec, new Vector2(0.5f, 0.5f));
        this.item = item;
        onClick.AddListener(TakeItem);
    }

    private void TakeItem() {
        item.Take();
    }
}
