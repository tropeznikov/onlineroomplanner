﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ChangeInputValueButton : MonoBehaviour, IPointerClickHandler {
    public InputField inputField;
    public int delta;

    public void OnPointerClick(PointerEventData eventData) {
        if (inputField) {
            if (inputField.contentType != InputField.ContentType.IntegerNumber) {
                Debug.Log("Неверный ContentType у поля ввода!");
                return;
            }

            inputField.Select();

            int value;
            if (!int.TryParse(inputField.text, out value)) {
                return;
            }
            value += delta;
            inputField.text = value.ToString();
            StartCoroutine(DeselectInputField());
        }
    }

    IEnumerator MoveTextEnd_NextFrame() {
        yield return 0;
        inputField.MoveTextEnd(false);
    }

    IEnumerator DeselectInputField() {
        yield return 0;
        EventSystem.current.SetSelectedGameObject(null);
    }
}
