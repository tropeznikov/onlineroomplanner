﻿using UnityEngine;

public class SingletonUI<T> : MonoBehaviour where T : MonoBehaviour {
    private static T instance;

    public static T Instance {
        get {
            if (!instance) {
                instance = FindObjectOfType<T>();
                if (!instance)
                    Debug.LogError("Ошибка: Сцена не содержит объекта с компонентом " + typeof(T).ToString());
            }
            return instance;
        }
    }
}
