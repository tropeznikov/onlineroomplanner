﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ParamInputPanel : MonoBehaviour {
    public Text nameText;
    public InputField inputField;
    public Text measureText;
    public ChangeInputValueButton increaseValueButton;
    public ChangeInputValueButton decreaseValueButton;

    private string measure;
    private int defaulValue;
    private int prevValue;
    private SetValueMethod valueSetter;
    public delegate bool SetValueMethod(float f);

    public void Init(string name, float value, string measure, int delta, SetValueMethod valueSetter) {
        nameText.text = name;
        if (measure == "см")
            value = value * 100;
        defaulValue = Convert.ToInt32(value);
        inputField.text = defaulValue.ToString();
        prevValue = defaulValue;
        measureText.text = " " + measure;
        this.measure = measure;
        increaseValueButton.delta = delta;
        decreaseValueButton.delta = -delta;
        this.valueSetter = valueSetter;
        inputField.onEndEdit.AddListener(CheckValue);
    }

    public void CheckValue(string strValue) {
        float value;
        if (!float.TryParse(strValue, out value)) {
            ModalPanel.Instance.Warning("Значение поля " + nameText.text + " имеет недопустимый формат!");
            inputField.text = prevValue.ToString();
            StartCoroutine(Deselect());
            return;
        }

        if (measure == "см")
            value = value / 100f;

        if (!valueSetter(value)) {
            inputField.text = prevValue.ToString();
            StartCoroutine(Deselect());
            return;
        }
        else {
            if (measure == "см")
                value = value * 100;
            prevValue = Convert.ToInt32(value);
        }
    }

    public void Discard() {
        if (measure == "см")
            valueSetter(defaulValue / 100f);
        else
            valueSetter(defaulValue);
    }

    IEnumerator Deselect() {
        yield return 0;
        EventSystem.current.SetSelectedGameObject(null);
    }
}
