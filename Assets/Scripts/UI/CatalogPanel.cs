﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CatalogPanel : SingletonUI<CatalogPanel> {
    public Catalog catalog;
    public Dropdown selector;
    public Text selectorLabel;
    public RectTransform content;
    public Button closeButton;
    public GameObject catalogPanelObject;

    public void Show() {
        BringToFront();
        catalogPanelObject.SetActive(true);
    }

    private void BringToFront() {
        catalogPanelObject.transform.SetAsLastSibling();
    }

    private void Hide() {
        catalogPanelObject.SetActive(false);
    }

    private void Start() {
        selector.options.Clear();
        selectorLabel.text = "не выбрано";
        closeButton.onClick.AddListener(Hide);
        selector.onValueChanged.AddListener(OnCategorySelect);
        StartCoroutine(LoadCategories());
    }

    private void OnCategorySelect(int categoryId) {
        foreach (Transform child in content)
            Destroy(child.gameObject);

        StartCoroutine(LoadItems(categoryId));
    }

    private IEnumerator LoadCategories() {
        CoroutineWithData cd = new CoroutineWithData(this, catalog.GetCategoryNames());
        yield return cd.coroutine;
        string[] categoryNames = cd.result as string[];
        if (categoryNames != null) {
            foreach (var name in categoryNames)
                selector.options.Add(new Dropdown.OptionData(name));
        }
        else {
            Debug.LogError("Не удалось загрузить названия категорий");
        }
    }

    private IEnumerator LoadItems(int categoryId) {
        CoroutineWithData cd = new CoroutineWithData(this, catalog.GetCategoryItemsData(categoryId));
        yield return cd.coroutine;
        string[] itemsData = cd.result as string[];
        if (itemsData != null) {
            for (int i = 0; i < itemsData.Length; i++) {
                cd = new CoroutineWithData(this, catalog.GetCategoryItem(categoryId, itemsData[i]));
                yield return cd.coroutine;
                CatalogItem item = cd.result as CatalogItem;
                if (item != null) {
                    RectTransform buttonRect = Instantiate(ResHolder.Instance.CatalogItemButtonPrefab).GetComponent<RectTransform>();
                    buttonRect.SetParent(content, false);
                    CatalogItemButton itemButton = buttonRect.GetComponent<CatalogItemButton>();
                    itemButton.Init(item);
                }
                else {
                    Debug.LogError("Не удалось загрузить элемент " + i + " категории " + categoryId);
                }
            }
        }
        else {
            Debug.LogError("Не удалось загрузить данные элементов категори " + categoryId);
        }
    }
}
