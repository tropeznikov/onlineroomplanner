﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ParamDialogWindow : SingletonUI<ParamDialogWindow> {
    public GameObject dialogWindowObject;
    public Text titleText;
    public Button closeButton;
    public Image image;
    public Button changeImageButton;
    public GameObject parametersPanel;
    public Button okButton;
    public Button cancelButton;
    public Button deleteButton;
    private UnityAction deleteEvent;
    private bool isOpened = false;

    public bool IsOpened {
        get { return isOpened; }
    }

    public void Open(string title, Texture2D img, RectTransform[] panels, UnityAction deleteEvent, UnityAction imageChangeEvent = null) {
        if (isOpened)
            Close();

        Show();

        titleText.text = title;
        Rect rec = new Rect(0, 0, img.width, img.height);
        image.sprite = Sprite.Create(img, rec, new Vector2(0.5f, 0.5f));

        foreach (var rect in panels)
            rect.SetParent(parametersPanel.transform, false);

        this.deleteEvent = deleteEvent;

        if (imageChangeEvent != null) {
            changeImageButton.onClick.RemoveAllListeners();
            changeImageButton.onClick.AddListener(imageChangeEvent);
            changeImageButton.gameObject.SetActive(true);
        }
        else {
            changeImageButton.gameObject.SetActive(false);
        }

        isOpened = true;
    }

    public void Close() {
        dialogWindowObject.SetActive(false);

        foreach (Transform child in parametersPanel.transform)
            Destroy(child.gameObject);

        isOpened = false;
    }

    private void OnDeleteBtnClick() {
        ModalPanel.Instance.Question("Вы действительно хотите удалить объект?", DeleteObject, () => { });
    }

    private void DeleteObject() {
        deleteEvent();
        Close();
    }

    private void DiscardInput() {
        ParamInputPanel[] inputPanels = parametersPanel.GetComponentsInChildren<ParamInputPanel>();
        foreach (var inputPanel in inputPanels) {
            inputPanel.Discard();
        }
    }

    private void Show() {
        BringToFront();
        dialogWindowObject.SetActive(true);
    }

    private void BringToFront() {
        dialogWindowObject.transform.SetAsLastSibling();
    }

    private void Start() {
        dialogWindowObject.SetActive(false);
        closeButton.onClick.AddListener(Close);
        okButton.onClick.AddListener(Close);
        cancelButton.onClick.AddListener(DiscardInput);
        cancelButton.onClick.AddListener(Close);
        deleteButton.onClick.AddListener(OnDeleteBtnClick);
    }
}
