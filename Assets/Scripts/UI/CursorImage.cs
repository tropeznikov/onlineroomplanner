﻿using UnityEngine;
using UnityEngine.UI;

public class CursorImage : SingletonUI<CursorImage> {
    public RectTransform rect;
    public Image image;
    private bool visible;
    private int offsetX;
    private int offsetY;

    public void SetImage(int width, int height, Texture2D texture, int offsetX, int offsetY) {
        image.rectTransform.sizeDelta = new Vector2(width, height);
        Rect rec = new Rect(0, 0, texture.width, texture.height);
        image.sprite = Sprite.Create(texture, rec, new Vector2(0.5f, 0.5f));
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public void Show() {
        BringToFront();
        visible = true;
        image.gameObject.SetActive(true);
    }

    public void Hide() {
        image.gameObject.SetActive(false);
        visible = false;
    }

    private void BringToFront() {
        gameObject.transform.SetAsLastSibling();
    }

    private void Start() {
        Hide();
        rect.anchorMin = new Vector2(0, 0);
        rect.anchorMax = new Vector2(0, 0);
        rect.pivot = new Vector2(0, 0);
        image.rectTransform.anchorMin = new Vector2(0, 0);
        image.rectTransform.anchorMax = new Vector2(0, 0);
        image.rectTransform.pivot = new Vector2(0, 0);
    }

    private void Update() {
        if (visible)
            rect.anchoredPosition = new Vector2(Input.mousePosition.x + offsetX, Input.mousePosition.y + offsetY);
    }
}
