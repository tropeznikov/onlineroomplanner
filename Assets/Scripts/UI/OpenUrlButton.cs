﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

public class OpenUrlButton : Button, IPointerDownHandler {
    public string url = "";

    public override void OnPointerDown(PointerEventData eventData) {
        base.OnPointerDown(eventData);
        if (!string.IsNullOrEmpty(url)) {
            OpenLinkJSPlugin(url);
            Debug.Log("Open Url");
        }
    }

    private void OpenLinkJSPlugin(string url) {
#if !UNITY_EDITOR
        openWindow(url);
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
