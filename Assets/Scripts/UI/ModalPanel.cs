﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ModalPanel : SingletonUI<ModalPanel> {
    public Text question;
    public OpenUrlButton okButton;
    public Button yesButton;
    public Button noButton;
    public Button cancelButton;
    public GameObject modalPanelObject;

    public void Warning(string text) {
        ShowPanel();
        question.text = text;

        okButton.onClick.RemoveAllListeners();
        okButton.onClick.AddListener(ClosePanel);

        okButton.gameObject.SetActive(true);
        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        cancelButton.gameObject.SetActive(false);
    }

    public void OpenUrlWindow(string text, string url) {
        ShowPanel();
        question.text = text;
        okButton.url = url;

        okButton.onClick.RemoveAllListeners();
        okButton.onClick.AddListener(ClearUrl);
        okButton.onClick.AddListener(ClosePanel);

        okButton.gameObject.SetActive(true);
        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        cancelButton.gameObject.SetActive(false);
    }



    public void Question(string text, UnityAction yesEvent, UnityAction noEvent, UnityAction cancelEvent = null) {
        ShowPanel();
        question.text = text;

        yesButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(yesEvent);
        yesButton.onClick.AddListener(ClosePanel);

        noButton.onClick.RemoveAllListeners();
        noButton.onClick.AddListener(noEvent);
        noButton.onClick.AddListener(ClosePanel);

        okButton.gameObject.SetActive(false);
        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);

        if (cancelEvent != null) {
            cancelButton.onClick.RemoveAllListeners();
            cancelButton.onClick.AddListener(cancelEvent);
            cancelButton.onClick.AddListener(ClosePanel);
            cancelButton.gameObject.SetActive(true);
        }
        else {
            cancelButton.gameObject.SetActive(false);
        }
    }

    private void ClearUrl() {
        okButton.url = "";
    }

    private void ShowPanel() {
        BringPanelToFront();
        modalPanelObject.SetActive(true);
    }

    private void ClosePanel() {
        modalPanelObject.SetActive(false);
    }

    private void BringPanelToFront() {
        modalPanelObject.transform.SetAsLastSibling();
    }

    private void OnEnable() {
        BringPanelToFront();
    }

    private void Awake() {
        ClosePanel();
    }
}
