﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler {
    private Vector2 pointerOffset;
    private RectTransform canvasRectTransform;
    private RectTransform panelRectTransform;

    void Awake() {
        Canvas canvas = GetComponentInParent<Canvas>();
        if (canvas != null) {
            canvasRectTransform = canvas.transform as RectTransform;
            panelRectTransform = transform as RectTransform;
        }
    }

    public void OnPointerDown(PointerEventData data) {
        //panelRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            panelRectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }

    public void OnDrag(PointerEventData data) {
        if (panelRectTransform == null)
            return;

        Vector2 pointerPostion = ClampToWindow(data);

        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvasRectTransform, pointerPostion, data.pressEventCamera, out localPointerPosition)
        ) {
            panelRectTransform.localPosition = localPointerPosition - pointerOffset;
        }
    }

    Vector2 ClampToWindow(PointerEventData data) {
        Vector2 rawPointerPosition = data.position;

        Vector3[] canvasCorners = new Vector3[4];
        canvasRectTransform.GetWorldCorners(canvasCorners);

        float width = panelRectTransform.rect.width;
        float height = panelRectTransform.rect.height;
        float xMin = canvasCorners[0].x + width * panelRectTransform.pivot.x + pointerOffset.x;
        float xMax = canvasCorners[2].x - width * (1 - panelRectTransform.pivot.x) + pointerOffset.x;
        float yMin = canvasCorners[0].y + height * panelRectTransform.pivot.y + pointerOffset.y;
        float yMax = canvasCorners[2].y - height * (1 - panelRectTransform.pivot.y) + pointerOffset.y;

        float clampedX = Mathf.Clamp(rawPointerPosition.x, xMin, xMax);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, yMin, yMax);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }
}
