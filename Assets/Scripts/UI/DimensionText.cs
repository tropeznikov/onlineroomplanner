﻿using UnityEngine;
using UnityEngine.UI;

public class DimensionText : Text {

    private Vector3 position;
    private bool visible = true;

    public Vector3 Position {
        set {
            position = value;
            UpdateScreenPos();
        }
    }

    public bool Visible {
        get { return visible; }
        set {
            visible = value;
            gameObject.SetActive(value);
            if (value)
                UpdateScreenPos();
        }
    }

    public void Init() {
        gameObject.transform.SetParent(GameObject.Find("DimensionTexts").transform, false);
        text = "";
        alignment = TextAnchor.MiddleCenter;
        rectTransform.sizeDelta = new Vector2(52, 16);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        color = Color.black;
        font = Resources.Load("Fonts/arial", typeof(Font)) as Font;
    }

    public void Destroy() {
        Destroy(gameObject);
    }

    private void UpdateScreenPos() {
        Vector3 textScreenPos = Camera.main.WorldToScreenPoint(position);
        rectTransform.anchoredPosition = new Vector2(textScreenPos.x, textScreenPos.y);
    }

    new public void OnEnable() {
        base.OnEnable();
        Camera2D.OnMoved += UpdateScreenPos;
    }

    new public void OnDisable() {
        base.OnDisable();
        Camera2D.OnMoved -= UpdateScreenPos;
    }
}
