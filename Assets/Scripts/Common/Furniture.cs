﻿using UnityEngine;

public abstract class Furniture : MonoBehaviour {

    protected Room room;
    protected int id;
    protected int categoryId;
    new protected string name;
    protected float length;
    protected float width;
    protected float height;
    protected float distFromFloor;
    protected FurnitureRes resource;
    protected bool isSelected;

    public delegate void ClickAction(MonoBehaviour sender);
    public static event ClickAction OnClicked = delegate { };

    #region свойства
    public Room Room {
        get { return room; }
        set {
            if (room == null) {
                room = value;
                room.Furniture.Add(this);
                gameObject.transform.name = name + Number;
                gameObject.transform.parent = room.gameObject.transform;
            }
        }
    }

    public int Id {
        get { return id; }
    }

    public int CategoryId {
        get { return categoryId; }
    }

    public string Name {
        get { return name != null ? name : ""; }
    }

    public int Number {
        get {
            if (room != null) {
                return room.Furniture.IndexOf(this);
            }
            else {
                Debug.LogError(name + ": помещение не задано");
                return -1;
            }
        }
    }

    public float RotationAngle {
        get { return transform.rotation.eulerAngles.y; }
        set {
            transform.rotation = Quaternion.Euler(0f, value % 360, 0f);
        }
    }

    public abstract float Length {
        get;
        protected set;
    }

    public abstract float Width {
        get;
        protected set;
    }

    public abstract float Height {
        get;
        protected set;
    }

    public abstract float DistFromFloor {
        get;
        protected set;
    }

    public FurnitureRes Resource {
        get { return resource; }
    }

    public Texture2D PerspImage {
        get { return resource != null ? resource.PerspImage : ResHolder.Instance.NoTexture; }
    }
    #endregion

    #region методы
    public virtual void Init(Room room, Vector3 position, FurnitureRes resource) {
        gameObject.transform.position = position;
        id = resource.Id;
        categoryId = resource.CategoryId;
        name = resource.Name;
        Room = room;
        Length = resource.DefaultLength;
        Width = resource.DefaultWidth;
        Height = resource.DefaultHeight;
        DistFromFloor = resource.DefaultDistFromFloor;
        this.resource = resource;

    }

    public virtual bool Init(Room room, FurnitureData data) {
        gameObject.transform.position = new Vector3(data.Position.x, 0f, data.Position.z);
        gameObject.transform.rotation = Quaternion.Euler(0f, data.Rotation.eulerAngles.y, 0f);
        id = data.Id;
        categoryId = data.CategoryId;
        name = data.Name;
        Room = room;
        resource = data.Resource;
        Length = data.Length;
        Width = data.Width;
        Height = data.Height;
        DistFromFloor = data.DistFromFloor;
        return true;
    }

    public void RotateTo(float degree) {
        gameObject.transform.rotation = Quaternion.Euler(0f, degree, 0f);
    }

    public void RotateBy(float degree) {
        gameObject.transform.Rotate(0f, degree, 0f, Space.World);
    }

    public virtual void Destroy() {
        room.Furniture.Remove(this);
        Destroy(gameObject);
    }

    public virtual void OnMouseUpAsButton() {
        if (isSelected)
            Deselect();
        else
            Select();
    }

    public virtual void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            Deselect();
            OnClicked(this);
            ShowParamWindow();
        }
    }

    public void DeselectExceptSender(MonoBehaviour sender) {
        if (isSelected && sender != this)
            Deselect();
    }

    protected virtual void Select() {
        isSelected = true;
        OnClicked(this);

        if (ParamDialogWindow.Instance.IsOpened)
            ParamDialogWindow.Instance.Close();
    }

    protected virtual void Deselect() {
        isSelected = false;
    }

    protected bool SetLength(float value) {
        float defaultLength = resource.DefaultLength;
        if (value < defaultLength * 0.5 || value > defaultLength * 2) {
            ModalPanel.Instance.Warning("Длина объекта может отличаться от исходного значения не более чем в 2 раза!");
            return false;
        }
        else {
            Length = value;
            return true;
        }
    }

    protected bool SetWidth(float value) {
        float defaultWidth = resource.DefaultWidth;
        if (value < defaultWidth * 0.5 || value > defaultWidth * 2) {
            ModalPanel.Instance.Warning("Ширина объекта может отличаться от исходного значения не более чем в 2 раза!");
            return false;
        }
        else {
            Width = value;
            return true;
        }
    }

    protected bool SetHeight(float value) {
        float defaultHeight = resource.DefaultHeight;
        if (value < defaultHeight * 0.5 || value > defaultHeight * 2) {
            ModalPanel.Instance.Warning("Высота объекта может отличаться от исходного значения не более чем в 2 раза!");
            return false;
        }
        else {
            Height = value;
            return true;
        }
    }

    protected bool SetDistFromFloor(float value) {
        if (value < -3 || value > 5) {
            ModalPanel.Instance.Warning("Недопустимое значение расстояния от пола!");
            return false;
        }
        else {
            DistFromFloor = value;
            return true;
        }
    }

    protected bool SetRotationAngle(float value) {
        RotationAngle = value;
        return true;
    }

    protected void ShowParamWindow() {
        RectTransform[] panels = new RectTransform[5];
        for (int i = 0; i < panels.Length; i++)
            panels[i] = Instantiate(ResHolder.Instance.ParamInputPanelPrefab).GetComponent<RectTransform>();

        panels[0].GetComponent<ParamInputPanel>().Init("Длина", Length, "см", 1, SetLength);
        panels[1].GetComponent<ParamInputPanel>().Init("Ширина", Width, "см", 1, SetWidth);
        panels[2].GetComponent<ParamInputPanel>().Init("Высота", Height, "см", 1, SetHeight);
        panels[3].GetComponent<ParamInputPanel>().Init("От пола", DistFromFloor, "см", 1, SetDistFromFloor);
        panels[4].GetComponent<ParamInputPanel>().Init("Поворот", RotationAngle, "°", 5, SetRotationAngle);

        ParamDialogWindow.Instance.Open("Параметры объекта\n" + name, PerspImage, panels, Destroy);
    }

    protected virtual void OnEnable() {
        OnClicked += DeselectExceptSender;
    }

    protected virtual void OnDisable() {
        OnClicked -= DeselectExceptSender;
    }

    protected virtual void Update() {
        if (isSelected && Input.GetKeyUp(KeyCode.Delete)) {
            Destroy();
        }
    }
    #endregion
}
