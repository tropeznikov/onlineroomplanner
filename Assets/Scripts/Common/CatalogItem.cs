﻿using UnityEngine;
using System.Collections;

public abstract class CatalogItem {

    public delegate void ItemTakenAction(CatalogItem item);
    public static event ItemTakenAction OnItemTaken = delegate { };

    protected int id;
    protected string name;
    protected CatalogCategory category;

    public int Id {
        get { return id; }
    }

    public int CategoryId {
        get { return category.Id; }
    }

    public string CategoryName {
        get { return category.Name; }
    }

    public string Name {
        get { return name; }
    }

    public abstract Texture2D Image {
        get;
    }

    public CatalogItem(CatalogCategory category, int id, string name) {
        this.category = category;
        this.id = id;
        this.name = name;
    }

    public virtual void Take() {
        OnItemTaken(this);
    }
}
