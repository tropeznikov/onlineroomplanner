﻿using UnityEngine;

public class ResHolder : Singleton<ResHolder> {
    Material floorMaterial = Resources.Load<Material>("Materials/lightgrey");
    Material pointMaterial = Resources.Load<Material>("Materials/gray_alpha");
    Material wallMaterial = Resources.Load<Material>("Materials/silver");
    Material doorMaterial = Resources.Load<Material>("Materials/brown");
    Material doorSelectedMaterial = Resources.Load<Material>("Materials/light_brown");
    Material windowMaterial = Resources.Load<Material>("Materials/blue");
    Material windowSelectedMaterial = Resources.Load<Material>("Materials/light_blue");
    Material blackLineMaterial = Resources.Load<Material>("Materials/black");
    Material grayLineMaterial = Resources.Load<Material>("Materials/gray_alpha");
    Material alertMaterial = Resources.Load<Material>("Materials/alert");

    Texture2D noTexture = Resources.Load<Texture2D>("Textures/no_texture");
    GameObject noModel = Resources.Load<GameObject>("Models/no_model");
    GameObject model2D = Resources.Load<GameObject>("Models/cube2d");

    Texture2D[] windowTextures = Resources.LoadAll<Texture2D>("Textures/Windows");
    Texture2D[] doorTextures = Resources.LoadAll<Texture2D>("Textures/Doors");
    Texture2D[] windowPictures = Resources.LoadAll<Texture2D>("Textures/Windows/Pics");
    Texture2D[] doorPictures = Resources.LoadAll<Texture2D>("Textures/Doors/Pics");

    Texture2D windowIconGrey = Resources.Load<Texture2D>("Icons/window_icon_grey");
    Texture2D windowIconColor = Resources.Load<Texture2D>("Icons/window_icon_color");
    Texture2D doorIconGrey = Resources.Load<Texture2D>("Icons/door_icon_grey");
    Texture2D doorIconColor = Resources.Load<Texture2D>("Icons/door_icon_color");

    GameObject paramInputPanelPrefab = Resources.Load<GameObject>("UI/ParameterInputPanel");
    GameObject catalogItemButtonPrefab = Resources.Load<GameObject>("UI/CatalogItemButton");

    Font arialFont = Resources.Load<Font>("Fonts/arial");

    #region свойства
    public Texture2D NoTexture {
        get { return noTexture; }
    }

    public GameObject NoModel {
        get { return noModel; }
    }

    public GameObject Model2D {
        get { return model2D; }
    }

    public Texture2D[] DoorTextures {
        get { return doorTextures; }
    }

    public Texture2D[] WindowTextures {
        get { return windowTextures; }
    }

    public Texture2D[] WindowPictures {
        get { return windowPictures; }
    }

    public Texture2D[] DoorPictures {
        get { return doorPictures; }
    }

    public Texture2D WindowIconGrey {
        get { return windowIconGrey; }
    }

    public Texture2D WindowIconColor {
        get { return windowIconColor; }
    }

    public Texture2D DoorIconGrey {
        get { return doorIconGrey; }
    }

    public Texture2D DoorIconColor {
        get { return doorIconColor; }
    }

    public Material FloorMaterial {
        get { return floorMaterial; }
    }

    public Material PointMaterial {
        get { return pointMaterial; }
    }

    public Material WallMaterial {
        get { return wallMaterial; }
    }
    public Material DoorMaterial {
        get { return doorMaterial; }
    }

    public Material DoorSelectedMaterial {
        get { return doorSelectedMaterial; }
    }

    public Material WindowMaterial {
        get { return windowMaterial; }
    }

    public Material WindowSelectedMaterial {
        get { return windowSelectedMaterial; }

    }

    public Material BlackLineMaterial {
        get { return blackLineMaterial; }
    }

    public Material GrayLineMaterial {
        get { return grayLineMaterial; }
    }

    public Material AlertMaterial {
        get { return alertMaterial; }
    }

    public GameObject ParamInputPanelPrefab {
        get { return paramInputPanelPrefab; }
    }

    public GameObject CatalogItemButtonPrefab {
        get { return catalogItemButtonPrefab; }
    }

    public Font ArialFont {
        get { return arialFont; }
    }
    #endregion
}