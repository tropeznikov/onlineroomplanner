﻿public class CatalogCategory {

    private int id;
    private string name;
    private Catalog catalog;

    public string Name {
        get { return name; }
    }

    public int Id {
        get { return id; }
    }

    public string CatalogName {
        get { return catalog.GetType().ToString(); }
    }

    public CatalogCategory(Catalog catalog, int id, string name) {
        this.catalog = catalog;
        this.id = id;
        this.name = name;
    }
}
