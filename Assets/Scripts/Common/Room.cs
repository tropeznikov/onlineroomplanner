﻿using UnityEngine;
using System.Collections.Generic;

// абстрактный базовый класс для помещений
public abstract class Room : MonoBehaviour {

    protected List<Furniture> furniture; // объекты мебели

    public List<Furniture> Furniture {
        get { return furniture; }
    }
}
