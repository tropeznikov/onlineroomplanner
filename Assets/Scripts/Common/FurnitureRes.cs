﻿using UnityEngine;

public class FurnitureRes : CatalogItem {
    private float defaultLength;
    private float defaultWidth;
    private float defaultHeight;
    private float defaultDistFromFloor;
    private Texture2D perspImage;
    private Texture2D topImage;
    private GameObject model;

    #region свойства
    public float DefaultLength {
        get { return defaultLength; }
    }

    public float DefaultWidth {
        get { return defaultWidth; }
    }

    public float DefaultHeight {
        get { return defaultHeight; }
    }

    public float DefaultDistFromFloor {
        get { return defaultDistFromFloor; }
    }

    public Texture2D PerspImage {
        get { return perspImage; }
    }

    public Texture2D TopImage {
        get { return topImage; }
    }

    public GameObject Model {
        get { return model; }
    }

    public override Texture2D Image {
        get { return perspImage; }
    }
    #endregion

    public FurnitureRes(CatalogCategory category, int id, string name, float length, float width, float height,
        float distFromFloor, Texture2D perspImage, Texture2D topImage, GameObject model) : base(category, id, name) {
        defaultLength = length;
        defaultWidth = width;
        defaultHeight = height;
        defaultDistFromFloor = distFromFloor;

        if (perspImage) {
            this.perspImage = perspImage;
        }
        else {
            this.perspImage = ResHolder.Instance.NoTexture;
            Debug.LogError(name + ": не удалось загрузить изображение (пекспектива)");
        }

        if (topImage) {
            this.topImage = topImage;
        }
        else {
            this.topImage = ResHolder.Instance.NoTexture;
            Debug.LogError(name + ": не удалось загрузить изображение (вид сверху)");
        }

        if (model) {
            this.model = model;
        }
        else {
            this.model = ResHolder.Instance.NoModel;
            Debug.LogError(name + ": не удалось загрузить 3D модель");
        }
    }
}