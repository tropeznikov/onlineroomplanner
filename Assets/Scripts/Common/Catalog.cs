﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Catalog : MonoBehaviour {
    public IEnumerator GetCategoryNames() {
        string url = Data2D.Instance.MainUrl + "get_category_names.php";
        WWWForm form = new WWWForm();
        form.AddField("catalog", GetType().Name);

        WWW www = new WWW(url, form);
        yield return www;

        if (!string.IsNullOrEmpty(www.error)) {
            Debug.LogError(www.error);
            yield return www.error;
        }
        else {
            JSONObject obj = new JSONObject(www.text);
            if (obj.type == JSONObject.Type.ARRAY) {
                List<string> list = new List<string>();
                foreach (JSONObject j in obj.list) {
                    if (j.type == JSONObject.Type.STRING)
                        list.Add(j.str);
                    else
                        Debug.LogError("GetCategoryNames: полученный JSONObject не является строкой");
                }
                yield return list.ToArray();
            }
            else {
                Debug.LogError("GetCategoryNames: полученный JSONObject не является массивом");
                yield return null;
            }
        }
    }

    public IEnumerator GetCategoryItemsData(int categoryId) {
        string url = Data2D.Instance.MainUrl + "get_category_items.php";
        WWWForm form = new WWWForm();
        form.AddField("catalog", GetType().Name);
        form.AddField("category_id", categoryId + 1);

        WWW www = new WWW(url, form);
        yield return www;

        if (!string.IsNullOrEmpty(www.error)) {
            Debug.LogError(www.error);
            yield return www.error;
        }
        else {
            JSONObject obj = new JSONObject(www.text);
            if (obj.type == JSONObject.Type.ARRAY) {
                List<string> list = new List<string>();
                foreach (JSONObject j in obj.list) {
                    string str = j.ToString();
                    list.Add(str);
                }
                yield return list.ToArray();
            }
            else {
                Debug.LogError("GetCategoryItemsData: Полученный JSONObject не является массивом");
                yield return null;
            }
        }
    }

    public abstract IEnumerator GetCategoryItem(int categoryId, string json_str);
}
