﻿using UnityEngine;
using System.Collections;

public class FurnitureCatalog : Catalog {
    public override IEnumerator GetCategoryItem(int categoryId, string json_str) {
        JSONObject obj = new JSONObject(json_str);
        if (obj.type == JSONObject.Type.OBJECT) {
            int count = 0;
            string itemName = "", categoryName = "", bundleDir = "", bundleName = "";
            int itemId = 0, length = 0, width = 0, height = 0, distFromFloor = 0;
            for (int i = 0; i < obj.list.Count; i++) {
                string key = (string)obj.keys[i];
                JSONObject j = (JSONObject)obj.list[i];
                switch (key) {
                    case "obj_name":
                        itemName = j.str;
                        count++;
                        break;
                    case "cat_name":
                        categoryName = j.str;
                        count++;
                        break;
                    case "dir_name":
                        bundleDir = j.str;
                        count++;
                        break;
                    case "asset_bundle":
                        bundleName = j.str;
                        count++;
                        break;
                    case "id":
                        itemId = int.Parse(j.str) - 1;
                        count++;
                        break;
                    case "length":
                        length = int.Parse(j.str);
                        count++;
                        break;
                    case "width":
                        width = int.Parse(j.str);
                        count++;
                        break;
                    case "height":
                        height = int.Parse(j.str);
                        count++;
                        break;
                    case "dist_from_floor":
                        distFromFloor = int.Parse(j.str);
                        count++;
                        break;
                }
            }
            if (count >= 9) {
                string url = Data2D.Instance.MainUrl + "as1/";
                AssetBundle bundle = AssetBundleManager.getAssetBundle(url, bundleDir + "/" + bundleName);
                if (!bundle) {
                    CoroutineWithData cd = new CoroutineWithData(this, DownloadAssetBundle(url, bundleDir + "/" + bundleName));
                    yield return cd.coroutine;
                    bundle = cd.result as AssetBundle;
                }

                if (bundle != null) {
                    GameObject model = null;
                    Texture2D perspImage = null;
                    Texture2D topImage = null;
                    UnityEngine.Object ueObj;

                    // загрузка 3D объекта из бандла
                    ueObj = bundle.LoadAsset(bundleName + "_prefab");
                    if (ueObj != null) {
                        model = ueObj as GameObject;
                        if (model == null)
                            Debug.LogError("Не удается привести " + bundleName + "_prefab к типу GameObject");
                    }
                    else {
                        Debug.LogError("Бандл не содержит ассет " + bundleName + "_prefab");
                    }

                    // загрузка изображения (пекспектива) из бандла
                    ueObj = bundle.LoadAsset(bundleName + "_persp");
                    if (ueObj != null) {
                        perspImage = ueObj as Texture2D;
                        if (perspImage == null)
                            Debug.LogError("Не удается привести " + bundleName + "_persp к типу Texture2D");
                    }
                    else {
                        Debug.LogError("Бандл не содержит ассет " + bundleName + "_persp");
                    }

                    // загрузка изображения (пекспектива) из бандла
                    ueObj = bundle.LoadAsset(bundleName + "_top");
                    if (ueObj != null) {
                        topImage = ueObj as Texture2D;
                        if (topImage == null)
                            Debug.LogError("Не удается привести " + bundleName + "_top к типу Texture2D");
                    }
                    else {
                        Debug.LogError("Бандл не содержит ассет " + bundleName + "_top");
                    }

                    if (model != null && perspImage != null && topImage != null) {
                        CatalogCategory category = new CatalogCategory(this, categoryId, categoryName);
                        CatalogItem item = new FurnitureRes(category, itemId, itemName, length / 100f,
                            width / 100f, height / 100f, distFromFloor / 100f, perspImage, topImage, model);
                        yield return item;
                    }
                }
                else {
                    Debug.LogError("Не удалось загрузить бандл!");
                }
            }
            else {
                Debug.LogError("GetCategoryItem: Неверное число параметров в JSONObject");
            }
        }
        else {
            Debug.LogError("GetCategoryItem: Полученный JSONObject не является объектом");
        }
    }

    IEnumerator DownloadAssetBundle(string url, string bundleName) {
        yield return StartCoroutine(AssetBundleManager.downloadAssetBundle(url, bundleName));
        yield return AssetBundleManager.getAssetBundle(url, bundleName);
    }
}