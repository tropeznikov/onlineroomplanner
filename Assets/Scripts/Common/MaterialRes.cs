﻿using UnityEngine;
using System.Collections;
using System;

public class MaterialRes : CatalogItem {
    private Texture2D texture;
    private int size;

    public override Texture2D Image {
        get { return texture; }
    }

    public Texture2D Texture {
        get { return texture; }
    }

    public int Size {
        get { return size; }
    }

    public MaterialRes(CatalogCategory category, int id, string name, Texture2D texture,
        int size) : base(category, id, name) {
        if (texture) {
            this.texture = texture;
        }
        else {
            this.texture = ResHolder.Instance.NoTexture;
            Debug.LogError(name + ": не удалось загрузить текстуру");
        }

        this.size = size;
    }
}
