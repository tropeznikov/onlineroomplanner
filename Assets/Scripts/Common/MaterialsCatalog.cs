﻿using UnityEngine;
using System.Collections;

public class MaterialsCatalog : Catalog {
    public override IEnumerator GetCategoryItem(int categoryId, string json_str) {
        JSONObject obj = new JSONObject(json_str);
        if (obj.type == JSONObject.Type.OBJECT) {
            int count = 0;
            string itemName = "", categoryName = "", textureDir = "", textureName = "";
            int itemId = 0, size = 0;
            for (int i = 0; i < obj.list.Count; i++) {
                string key = (string)obj.keys[i];
                JSONObject j = (JSONObject)obj.list[i];
                switch (key) {
                    case "mat_name":
                        itemName = j.str;
                        count++;
                        break;
                    case "cat_name":
                        categoryName = j.str;
                        count++;
                        break;
                    case "dir_name":
                        textureDir = j.str;
                        count++;
                        break;
                    case "texture":
                        textureName = j.str;
                        count++;
                        break;
                    case "id":
                        itemId = int.Parse(j.str) - 1;
                        count++;
                        break;
                    case "size_cm":
                        size = int.Parse(j.str);
                        count++;
                        break;
                }
            }
            if (count >= 6) {
                string url = Data2D.Instance.MainUrl + "img/" + textureDir + "/" + textureName;
                var cd = new CoroutineWithData(this, DownloadTexture(url));
                yield return cd.coroutine;
                Texture2D texture = cd.result as Texture2D;
                if (texture != null) {
                    CatalogCategory category = new CatalogCategory(this, categoryId, categoryName);
                    CatalogItem item = new MaterialRes(category, itemId, itemName, texture, size);
                    yield return item;
                }
                else {
                    Debug.LogError("Ошибка загрузки текстуры элемента " + itemId + " категории "
                        + categoryId + "\nURL текустуры: " + url);
                }
            }
            else {
                Debug.LogError("GetCategoryItem: Неверное число параметров в JSONObject");
            }
        }
        else {
            Debug.LogError("GetCategoryItem: Полученный JSONObject не является объектом");
        }
    }

    IEnumerator DownloadTexture(string url) {
        WWW www = new WWW(url);
        yield return www;

        if (!string.IsNullOrEmpty(www.error)) {
            Debug.LogError(www.error);
        }
        else {
            yield return www.texture;
        }
    }
}