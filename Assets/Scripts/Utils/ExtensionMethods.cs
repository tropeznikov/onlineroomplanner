﻿using UnityEngine;

// методы расширения для классов UnityEngine
public static class ExtensionMethods {
    public static void DrawArc(this LineRenderer line, Vector3 center, float radius, float startAngle, float arcAngle) {
        int segments = (int)arcAngle;
        line.SetVertexCount(segments + 1);
        float x;
        float y = center.y;
        float z;
        float angle = startAngle;

        for (int i = 0; i < (segments + 1); i++) {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius + center.x;
            z = Mathf.Cos(Mathf.Deg2Rad * angle) * radius + center.z;
            line.SetPosition(i, new Vector3(x, y, z));
            angle += 1;
        }
    }

    public static void DrawPolyLine(this LineRenderer line, Vector3[] points) {
        line.SetVertexCount(points.Length);
        for (int i = 0; i < points.Length; i++) {
            line.SetPosition(i, points[i]);
        }
    }

    public static void DrawLine(this LineRenderer line, Vector3 startPoint, Vector3 endPoint) {
        line.SetVertexCount(2);
        line.SetPosition(0, startPoint);
        line.SetPosition(1, endPoint);
    }
}
