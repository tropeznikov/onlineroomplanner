using UnityEngine;

public class MouseUtils {
    //����������� ������ ����
    public enum Button : int { Left = 0, Right = 1, Middle = 2, None = 3 }

    // ������� ���������� ����� �� ������� ��������� ������ ����
    // � ������� ������������ �� ���������� depth �� ������ 
    public static Vector3 GetMouseWorldPosAtDepth(float depth) {
        Vector3 mouseScreenPosition = Input.mousePosition;
        mouseScreenPosition.z = depth;
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
        return new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, mouseWorldPosition.z);
    }

    // ������� ���������� ����� �� ������� ��������� ������ ����
    // � ������� ������������ �� ������ �� ����������, ������ ���������� �� ������ �� �������
    public static Vector3 GetMouseWorldPosAtDistToGO(GameObject go) {
        // �������� ������ ������ �� ������ � ������ �������
        Vector3 vectFormCamera = go.transform.position - Camera.main.transform.position;

        // ������� �������� ����� ������� �� ������ ������ ������������ ������
        float distFromCamera = Vector3.Dot(vectFormCamera, Camera.main.transform.forward);

        // ������� ���������� ����� � 3D, ���� ��������� ������ ����
        return GetMouseWorldPosAtDepth(distFromCamera); ;
    }

    // ���������, ���������� �� �������� �� ������ � ���������� ����� ������ ���� ��� 
    // ���� �� 3D ��������, ���� �������� ������ � layerMask
    public static bool GetMouseWorldPosFromRaycast(int layerMask, out Vector3 pos) {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000, layerMask)) {
            pos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            return true;
        }
        else {
            pos = Vector3.zero;
            return false;
        }
    }
}