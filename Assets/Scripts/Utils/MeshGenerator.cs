﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

// режим генерации UV координат
public enum MeshTexMode { NoTexture, WrapRepeat, WrapClamp };

// генератор мешей
public static class MeshGenerator {
    // генерация меша с произвольным числом вершин
    public static Mesh GenerateMesh(List<Vector3> points, bool clockwise, Vector3 normal, MeshTexMode texMode) {
        Vector3[] vertices;
        int[] triangles;
        Vector2[] uvcoords;
        // если в метод передали 4 вершины, строим четырехугольных меш
        if (points.Count == 4) {
            vertices = points.ToArray(); // вершины
            triangles = new int[6]; // индексы вершин в треугольниках меша
            // первый треугольник
            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;
            // второй треугольник
            triangles[3] = 0;
            triangles[4] = 2;
            triangles[5] = 3;
        }
        // иначе строим меш с произвольным числом вершин
        else {
            Triangulation.GetResult(points, clockwise, normal, out vertices, out triangles);
        }

        //получаем текстурные координаты (uv-координаты)
        uvcoords = new Vector2[vertices.Length];
        switch (texMode) {
            case MeshTexMode.NoTexture:
                Vector2 emptyUV = new Vector2(0f, 0f);
                for (int i = 0; i < uvcoords.Length; i++)
                    uvcoords[i] = emptyUV;
                break;
            case MeshTexMode.WrapClamp:
                if (vertices.Length == 4) {
                    uvcoords[0] = new Vector2(0, 0);
                    uvcoords[1] = new Vector2(0, 1);
                    uvcoords[2] = new Vector2(1, 1);
                    uvcoords[3] = new Vector2(1, 0);
                    break;
                }
                else
                    goto case MeshTexMode.WrapRepeat;
            case MeshTexMode.WrapRepeat:
                var qtomz = Quaternion.FromToRotation(normal, Vector3.back);
                if (normal == Vector3.forward)
                    qtomz = Quaternion.Euler(0, 180, 0);
                uvcoords = vertices.Select(v => qtomz * v).Select(v => new Vector2(v.x, v.y)).ToArray();
                var minX = uvcoords.Min(v => v.x);
                var minY = uvcoords.Min(v => v.y);
                var baseuvcoord = uvcoords.OrderBy(v => v.x - minX + v.y - minY).FirstOrDefault();
                uvcoords = uvcoords.Select(v => v - baseuvcoord).ToArray();
                break;
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvcoords;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        return mesh;
    }
}
