﻿using UnityEngine;

public class Math3DFunc {
    // нахождение угла между векторами vector1 и vector1 при заданной нормали normal
    // результат: угол в интервале [-179,180]
    public static float AngleBetweenVectors(Vector3 vector1, Vector3 vector2, Vector3 normal) {
        Vector3 perpVector;
        float angle, signedAngle;

        // через произведение 1го вектора на нормаль находим перпендикуляр к 1му ввектору
        perpVector = Vector3.Cross(vector1, normal);

        // находим угол между векторами, результат в интервале [0,180]
        angle = Vector3.Angle(vector1, vector2);

        // находим знаковое значение угла между векторами, результат в интервале [-179,180]
        // умножаем угол на скалярное произведение 2го вектора и перпендикуляра
        // если угол между векторами <= 180, то результат положительный, если больше - отрицательный
        signedAngle = angle * Mathf.Sign(Vector3.Dot(perpVector, vector2));

        return signedAngle;
    }

    // нахождение угла между векторами vector1 и vector2 при заданной нормали normal
    // результат: угол в интервале [0,360]
    public static float Angle360BetweenVectors(Vector3 vector1, Vector3 vector2, Vector3 normal) {
        float signedAngle, angle360;
        signedAngle = AngleBetweenVectors(vector1, vector2, normal);

        // находим привычное значение угла между векторами, результат в интервале [0,360]
        angle360 = (signedAngle + 360) % 360;

        return angle360;
    }

    // нахождение площади мэша
    public static float MeshArea(Mesh mesh) {
        float area = 0;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        for (int i = 0; i < triangles.Length; i += 3) {
            Vector3 side1 = vertices[triangles[i + 1]] - vertices[triangles[i]];
            Vector3 side2 = vertices[triangles[i + 2]] - vertices[triangles[i]];
            Vector3 perp = Vector3.Cross(side1, side2);
            area += perp.magnitude / 2;
        }
        return area;
    }

    // нахождение проекции точки point на прямую, проходящую через точку linePoint
    // и параллельную вектору lineVec
    public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point) {
        Vector3 linePointToPoint = point - linePoint;
        float t = Vector3.Dot(linePointToPoint, lineVec);
        return linePoint + lineVec * t;
    }

    // нахождение проекции точки point на отрезок с точками linePoint1 и linePoint2
    // если проекция точки не лежит на отрезке, то возвращается клонец или начало отрезка
    public static Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point) {
        Vector3 vector = linePoint2 - linePoint1;
        Vector3 projectedPoint = ProjectPointOnLine(linePoint1, vector.normalized, point);
        int side = PointOnWhichSideOfLineSegment(linePoint1, linePoint2, projectedPoint);

        //проекция лежит на отрезке
        if (side == 0)
            return projectedPoint;
        else if (side == 1)
            return linePoint1;
        else if (side == 2)
            return linePoint2;
        else
            return Vector3.zero;
    }

    // Выяснение, лежит ли точка point на отрезке с точками linePoint1 и linePoint2, 
    // а если нет, то на какой стороне от отрезка она находится. Подразумевается, что
    // точка point лежит на прямой проходящей ерез точки linePoint1 и linePoint2
    // Возвращает 0, если точка лежит на отрезке
    // Возвращает 1, если точка не лежит на отрезке, но расположена ближе к linePoint1
    // Возвращает 2, если точка не лежит на отрезке, но расположена ближе к linePoint2
    public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point) {
        Vector3 lineVec = linePoint2 - linePoint1;
        Vector3 pointVec = point - linePoint1;
        float dot = Vector3.Dot(pointVec, lineVec);

        //точка point ближе к linePoint2, чем к linePoint1
        if (dot > 0) {
            //точка лежит на отрезке
            if (pointVec.magnitude <= lineVec.magnitude)
                return 0;
            //точка не лежит на отрезке
            else
                return 2;
        }
        //точка point ближе к linePoint1, чем к linePoint2 и
        //точка не лежит на отрезке с точками linePoint1 и linePoint2
        else {
            return 1;
        }
    }
}
