﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

static public class AssetBundleManager {
    // словарь для хранения ссылок на AssetBundles
    static private Dictionary<string, AssetBundleRef> dictAssetBundleRefs;

    static AssetBundleManager() {
        dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();
    }
    // класс со ссылкой на AssetBundle и его url
    private class AssetBundleRef {
        public AssetBundle assetBundle = null;
        public string url;
        public AssetBundleRef(string strUrlIn) {
            url = strUrlIn;
        }
    };

    // получение AssetBundle
    public static AssetBundle getAssetBundle(string abDirUrl, string bundlePath) {
        string keyName = abDirUrl + bundlePath;
        AssetBundleRef abRef;
        if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
            return abRef.assetBundle;
        else
            return null;
    }
    // загрузка AssetBundle
    public static IEnumerator downloadAssetBundle(string abDirUrl, string bundlePath) {
        string keyName = abDirUrl + bundlePath;
        if (dictAssetBundleRefs.ContainsKey(keyName))
            yield return null;
        else {
            while (!Caching.ready)
                yield return null;

            Hash128 hash;
            using (WWW www = new WWW(abDirUrl + "AssetBundlesWebGL")) {
                yield return www;
                if (!string.IsNullOrEmpty(www.error)) {
                    Debug.LogError(www.error);
                    yield break;
                }

                AssetBundleManifest manifest = www.assetBundle.LoadAsset("AssetBundleManifest") as AssetBundleManifest;
                hash = manifest.GetAssetBundleHash(bundlePath);

                www.assetBundle.Unload(false);
            }

            using (WWW www = WWW.LoadFromCacheOrDownload(abDirUrl + bundlePath, hash)) {
                yield return www;
                if (www.error != null)
                    throw new Exception("WWW download:" + www.error);
                AssetBundleRef abRef = new AssetBundleRef(abDirUrl + bundlePath);
                abRef.assetBundle = www.assetBundle;
                dictAssetBundleRefs.Add(keyName, abRef);
            }
        }
    }
    // выгрузка AssetBundle
    public static void Unload(string abDirUrl, string bundlePath, bool allObjects) {
        string keyName = abDirUrl + bundlePath;
        AssetBundleRef abRef;
        if (dictAssetBundleRefs.TryGetValue(keyName, out abRef)) {
            abRef.assetBundle.Unload(allObjects);
            abRef.assetBundle = null;
            dictAssetBundleRefs.Remove(keyName);
        }
    }
}