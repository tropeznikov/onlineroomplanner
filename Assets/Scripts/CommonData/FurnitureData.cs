﻿using UnityEngine;
using System.Collections;

public struct FurnitureData {

    private int id;
    private int categoryId;
    private string name;
    private float length;
    private float width;
    private float height;
    private float distFromFloor;
    private FurnitureRes resource;
    private Vector3 position;
    private Quaternion rotation;

    #region свойства
    public int Id {
        get { return id; }
    }

    public int CategoryId {
        get { return categoryId; }
    }

    public string Name {
        get { return name; }
    }

    public float Length {
        get { return length; }
    }

    public float Width {
        get { return width; }
    }

    public float Height {
        get { return height; }
    }

    public float DistFromFloor {
        get { return distFromFloor; }
    }

    public FurnitureRes Resource {
        get { return resource; }
    }

    public Vector3 Position {
        get { return position; }
    }

    public Quaternion Rotation {
        get { return rotation; }
    }
    #endregion

    public FurnitureData(Furniture furniture) {
        id = furniture.Id;
        categoryId = furniture.CategoryId;
        name = furniture.Name;
        length = furniture.Length;
        width = furniture.Width;
        height = furniture.Height;
        distFromFloor = furniture.DistFromFloor;
        resource = furniture.Resource;
        position = furniture.gameObject.transform.position;
        rotation = furniture.gameObject.transform.rotation;
    }
}
