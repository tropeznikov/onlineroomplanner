﻿using UnityEngine;

public class RoomData : Singleton<RoomData> {
    private WallData[] walls;
    private FurnitureData[] furniture;
    private Vector3 normal = Vector3.up;
    private float wallsHeight;
    private MaterialRes floorMaterialRes;
    private bool empty = true;

    #region свойства
    public WallData[] Walls {
        get { return walls; }
    }

    public FurnitureData[] Furniture {
        get { return furniture; }
    }

    public Vector3 Normal {
        get { return normal; }
    }

    public float WallsHeight {
        get { return wallsHeight; }
    }

    public MaterialRes FloorMaterialRes {
        get { return floorMaterialRes; }
    }

    public bool IsEmpty {
        get { return empty; }
    }
    #endregion

    public void SaveRoomData2D(Room2D room2d, float height) {
        empty = false;
        wallsHeight = height;

        walls = new WallData[room2d.Walls.Count];
        for (int i = 0; i < walls.Length; i++) {
            walls[i] = new WallData(room2d.Walls[i]);
        }

        furniture = new FurnitureData[room2d.Furniture.Count];
        for (int i = 0; i < furniture.Length; i++) {
            furniture[i] = new FurnitureData(room2d.Furniture[i]);
        }

        floorMaterialRes = room2d.FloorMaterialRes;
    }

    public void SaveRoomData3D(Room3D room3d) {
        for (int i = 0; i < room3d.Walls.Length; i++) {
            walls[i].MaterialRes = room3d.Walls[i].MaterialRes;
        }

        furniture = new FurnitureData[room3d.Furniture.Count];
        for (int i = 0; i < furniture.Length; i++) {
            furniture[i] = new FurnitureData(room3d.Furniture[i]);
        }

        floorMaterialRes = room3d.Floor.MaterialRes;
    }
}
