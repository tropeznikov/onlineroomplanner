﻿using UnityEngine;

public struct DoorData {
    Vector3 startPointPos;
    Vector3 endPointPos;
    float height;
    int textureID;

    public Vector3 StartPointPos {
        get { return startPointPos; }
    }

    public Vector3 EndPointPos {
        get { return endPointPos; }
    }
    public float Height {
        get { return height; }
    }

    public int TextureID {
        get { return textureID; }
    }

    public DoorData(Door2D door2d) {
        startPointPos = door2d.StartPointPos;
        endPointPos = door2d.EndPointPos;
        height = door2d.Height;
        textureID = door2d.TextureID;
    }
}
