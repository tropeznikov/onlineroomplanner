﻿using UnityEngine;
using System.Collections;

public struct WindowData {
    Vector3 startPointPos;
    Vector3 endPointPos;
    float height;
    float distOverFloor;
    int textureID;

    #region свойства
    public Vector3 StartPointPos {
        get { return startPointPos; }
    }

    public Vector3 EndPointPos {
        get { return endPointPos; }
    }
    public float Height {
        get { return height; }
    }

    public float DistOverFloor {
        get { return distOverFloor; }
    }

    public int TextureID {
        get { return textureID; }
    }
    #endregion

    public WindowData(Window2D window2d) {
        startPointPos = window2d.StartPointPos;
        endPointPos = window2d.EndPointPos;
        height = window2d.Height;
        distOverFloor = window2d.DistOverFloor;
        textureID = window2d.TextureID;
    }
}
