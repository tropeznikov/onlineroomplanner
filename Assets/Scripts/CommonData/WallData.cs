﻿using UnityEngine;
using System.Collections.Generic;

public struct WallData {
    private Vector3 startPointPos;
    private Vector3 endPointPos;
    private List<DoorData> doors;
    private List<WindowData> windows;
    private MaterialRes materialRes;

    #region свойства
    public Vector3 StartPointPos {
        get { return startPointPos; }
    }

    public Vector3 EndPointPos {
        get { return endPointPos; }
    }

    public List<DoorData> Doors {
        get { return doors; }
    }

    public List<WindowData> Windows {
        get { return windows; }
    }

    public MaterialRes MaterialRes {
        get { return materialRes; }
        set { materialRes = value; }
    }

    public WallData(Wall2D wall2d) {
        startPointPos = wall2d.StartPoint.Position;
        endPointPos = wall2d.EndPoint.Position;
        doors = new List<DoorData>();
        windows = new List<WindowData>();
        materialRes = wall2d.MaterialRes;

        foreach (WallOpening2D opn2d in wall2d.Openings) {
            if (opn2d is Door2D) {
                Door2D door2d = (Door2D)opn2d;
                doors.Add(new DoorData(door2d));
            }
            else if (opn2d is Window2D) {
                Window2D window2d = (Window2D)opn2d;
                windows.Add(new WindowData(window2d));
            }
        }
    }
    #endregion
}
